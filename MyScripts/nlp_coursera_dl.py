# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

from lxml import html
import requests as req

nlppage = req.get('https://class.coursera.org/nlp/lecture/preview')
page_tree = lxml.html.fromstring(nlppage.text) 
h3 = page_tree.xpath('//h3')
video_iframes = page_tree.xpath('//a[@class="lecture-link"]/@data-modal-iframe')
video_iframes[0]

# <codecell>

for ifr in video_iframes:
    vid_page = req.get(ifr)
    vid_page_tree = lxml.html.fromstring(vid_page.text)
    vid_link = vid_page_tree.xpath('//video/source[0]/@src')
    vid_page_tree

vid_link

# <codecell>


# <codecell>


# <codecell>


