"""mymath.py - our example math module"""

import os
PI = 3.14


def area(r):
    return (PI*r*r)

def volume(r=1):
    return PI*r**3

def circumference(r):
    return 2*PI*r

class Vehicle:
    def __init__(self, avgspeed=40):
        self.mileage = 0
        self.avgspeed = avgspeed
    def drive(self, hours):
        self.mileage += self.avgspeed*hours

    @staticmethod
    def impound():
        self.avgspeed = 0

# print(area(10))
sample_sentence = "To be or not to be that is the question"
occurs = {}

for word in sample_sentence.split():
    occurs[word] = occurs.get(word, 0) + 1

funcdic = {1:area, 2:volume}
print(funcdic[1](2))
print(os.getcwd())

v = Vehicle(70)
v.drive(1)
print(v.mileage)
v.drive(12)
print(v.mileage)




        
