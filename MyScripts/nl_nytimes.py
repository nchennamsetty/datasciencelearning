#Copyright Naren Chennamsetty 2013
#
# The purpose of this script is to read nytimes.com website and pull all articles
# for a given day and save it text format

#STEPS
# fetch nytimes homepage
# load it into beautiful soup

# <div class="story">
# <h3><a href="http://www.nytimes.com/2013/11/09/business/economy/us-unemployment-rate-rises-to-7-3-204000-jobs-added.html?hp">
# Private Hiring Brisk
# in Jobs Data Skewed
# by U.S. Shutdown</a></h3>
# <h6 class="byline">
# By NELSON D. SCHWARTZ        <span class="timestamp" data-eastern-timestamp=" 9:20 AM" data-utc-timestamp="1383920456000"></span>
# </h6>
# <p class="summary">
# Robust job creation in October — 204,000 positions — along with sizable upward revisions for the previous two months could influence the Federal Reserve’s thinking on when to ease its stimulus efforts.    </p>
# </div>
# save article 


import urllib #2 urllib is merged
import re
import sys
import os

def get_nytimes_frontpage():
	proxy = urllib.ProxyHandler({'http': '10.136.49.16:8080'})
	opener = urllib.build_opener(proxy)
	urllib.install_opener(opener)
	resp = urllib.urlopen("http://www.nytimes.com")

	frontpage = resp.read()
	print (frontpage)
	
	# pattern = 'Obama'
	# for line in html.split('\n'):
		# m = re.search(pattern, line)
		# if m:
			# print(line)
			
def main():
	get_nytimes_frontpage()
	
	
if __name__  == '__main__':
	main()
	
      
