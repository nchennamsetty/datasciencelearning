#!/usr/bin/python

from lxml import html
import lxml
import requests as req
from random import randint
import re

proxies = {
		  "http": "http://10.136.49.16:8080",
		  "https": "http://10.136.49.16:8080",
		}
cookies = {
		'CAUTH':'zcnW1qm0pUGIZSOVeTRuUONQwLPW4mcxSklIalQ3ivof9383nckfjpUvjDyzq3kV9lW6zTUggLfKnO--XD7_Kg.U6z0oR3N5_QceUz7p2Bp0A.2r7lLWAG-bR6pFXJRe8nSIrG3ILjKDlX0NtHwkxTBPRBXHLf6-qdqhn61cOxBtidgh4pIqt1TkJIXjMr9SOeVL4AqQ8JQLJ1fSei6CbUyzYXazQ0Z6em6TiU2k125sTt_dhl6D6P257XluapagxHFZNjhqdsBAq9dQalf2BWn1g'
		}

	
def get_page_tree(url, session):
	r  = session.get(url, proxies=proxies, verify=False , cookies = cookies )
	#r  = s.get(url,  verify=False , cookies = cookies )
	page_tree = lxml.html.fromstring(r.text)
	return page_tree

def get_lecture_pdfs(lecture_page_tree, session):
	pdfs = page_tree.xpath('//a[@title="PDF"]/@href')
	pattern = "https.*(Lecture\d+\.pdf)"

	for pdf in pdfs:
		print('Getting url %s\n', pdf)
		m = re.match(pattern, pdf)
		get_file(pdf, m.group(1), session)
		
		
def get_file(url, filename, session):
	resp = session.get(url, proxies=proxies, verify=False , cookies = cookies )
	f = open(filename, 'wb')
	for chunk in resp.iter_content(1024):
		if chunk:
			f.write(chunk)
	f.close()
		
	
def get_lecture_videos(lecture_page_tree, session):
    
	videos = lecture_page_tree.xpath('//a[@title="Video (MP4)"]/@href')
	for v in videos:
	    print("..downloading file...\n", v)
	    local_filename = 'Video' + v.split('=')[-1] + '.mp4'
	    get_file(v, local_filename, session)



def main():
	
	session = req.Session()
	page_tree = get_page_tree('https://class.coursera.org/ml-004/lecture', session)
	get_lecture_pdfs(page_tree,session)
	get_lecture_videos(page_tree, session)
    

if __name__  == '__main__':
   main()