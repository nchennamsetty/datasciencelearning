#!/bin/sh
#
# The following is a description of the optional instructor
# demo in chapter 11.
#
# The input to the job is the following file:
#
# ratings.csv:    comma-delimited textfile containing three
#		  columns (user ID, movie ID and the user's
#                 rating of that movie). This is the format
#                 that the Mahout job needs the data in 
#                 (though we could support other formats by  
#                 writing some Java code for Mahout).  This
#		  file was created by some very basic data
#		  munging of the data in HDFS from previous
#		  exercises.  
#
# Steps for the demo:
#
# 1. Add data to HDFS

hadoop fs -put ratings.csv /clouderamovies/demoratings.csv


# 2. Create the temporary directory in HDFS.  Note that 
#    this directory must either not exist or must exist
#    but be empty.  So that we don't fail if we need to
#    re-run the demo, we'll just remove and recreate it
#    every time.   Similarly, the output directory must
#    not already exist, so we'll remove it in all cases.

hadoop fs -rm -r -f /tmp/mahoutdemo
hadoop fs -mkdir /tmp/mahoutdemo

hadoop fs -rm -r -f /clouderamovies/demoresults

# 3.  Run Mahout's item-based recommender on the data you've 
#     put in HDFS.  We are going to use the Euclidean distance
#     similarity metric, but Mahout supports many others. See
#     org.apache.mahout.math.hadoop.similarity.SimilarityType
#     for a complete list (or just leave --similarityClassname
#     off the command and look at the list in the error message 
#     you'll get).

mahout recommenditembased \
   --input /clouderamovies/demoratings.csv \
   --tempDir /tmp/mahoutdemo \
   --similarityClassname SIMILARITY_EUCLIDEAN_DISTANCE \
   --output /clouderamovies/demoresults


# 4. This will take about three or four minutes to complete, 
#    Once it does, type the following command to view
#    the output:

hadoop fs -cat /clouderamovies/demoresults/part*


# Each line contains a user ID followed by a list of item ID
# and predicted rating pairs.  Here is an excerpt:
#
#   943	[1682:5.0,1681:5.0 ... ]
#
# which means that the user with ID 943 is likely to rate the 
# movie with ID 1682 as a 5.0, and the same goes for movie 1681.
#
# See the following article for more background on this demo:
#
#  http://www.cloudera.com/blog/2011/11/recommendation-with-apache-mahout-in-cdh3/
