#!/bin/bash

if [ ! -w "$PWD" ]; then
   echo "ERROR: This script requires the current directory to be writable"
   exit 1
fi

# Clean up the /clouderamovies directory in HDFS
cleanup() {
  # Things in hdfs:/clouderamovies, except for the 
  # access log, which we need to retain
  rm -f access.log
  hadoop fs -get /clouderamovies/access.log
  hadoop fs -rm -r -f /clouderamovies/*
  hadoop fs -put access.log /clouderamovies/access.log

  # leftovers from Mahout runs (in HDFS home directory)
  hadoop fs -rm -r -f temp
  hadoop fs -rm -r -f itemrecs_a
  hadoop fs -rm -r -f itemrecs_b

  # server processes that may have been started
  stop_bleats_server.sh
  stop_diff_server.sh
 
  # Java files created by Sqoop in the CWD
  find /home/training -type f -name 'movie.java' -exec rm {} \;
  find /home/training -type f -name 'user.java' -exec rm {} \;

  # Derby files left all over the place by Hive
  find /home/training -type f -name 'derby.log' -exec rm {} \;
  
  # Hive tables
  hive -e "drop table access_logs"
  hive -e "drop table bleats"
  hive -e "drop table bleatsfixed"
  hive -e "drop table bleater_ids"
  hive -e "drop table movies"
  hive -e "drop table users"
  hive -e "drop table sessions"
  hive -e "drop table json_sessions"
  hive -e "drop table bleated_rating_counts"
  hive -e "drop table word_rating_counts"
  hive -e "drop table mahout_input_a"
  hive -e "drop table mahout_input_b"
}

# Advance to Lab 0
lab0() {
  # No-op placeholder.  This lab is self-contained; it does
  # not rely on data from any previous lab nor does any
  # subsequent lab rely on data from it.
  cd ~/training_materials/data_science/exercises/lab0_toolintro/
}

# Advance to Lab1
lab1() {
  cd ~/training_materials/data_science/data/
  
  echo MovieLens Sqoop
  sqoop import --connect jdbc:mysql://localhost/movielens --table movie --fields-terminated-by '\t' --username training --password training --warehouse-dir /clouderamovies
  sqoop import --connect jdbc:mysql://localhost/movielens --table user --fields-terminated-by '\t' --username training --password training --warehouse-dir /clouderamovies

  echo Downloading Bleats
  start_bleats_server.sh
  sleep 20s
  /home/training/training_materials/data_science/exercises/lab1_dataimport/sample_solution/download_bleats.py
  sleep 10s
  stop_bleats_server.sh
  hadoop fs -mkdir /clouderamovies/bleats
  hadoop fs -put bleatsdownload.json /clouderamovies/bleats
}

# Advance to lab 2
lab2() {
  cd ~/training_materials/data_science/exercises/lab2_evaluatedata

  hadoop jar $STREAMJAR -D mapred.reduce.tasks=0 -input /clouderamovies/movie -output /clouderamovies/moviefixed -mapper moviemapper.py -file sample_solution/moviemapper.py;

  hadoop jar $STREAMJAR -D mapred.reduce.tasks=0 -input /clouderamovies/user -output /clouderamovies/userfixed -mapper usermapper.py -file sample_solution/usermapper.py;

  hadoop jar $STREAMJAR -D mapred.reduce.tasks=0 -input /clouderamovies/access.log -output /clouderamovies/accesslogfixed -mapper accesslogmapper.py -file sample_solution/accesslogmapper.py;
}

# Advance to Lab 3
lab3() {
  cd ~/training_materials/data_science/exercises/lab3_datatransform/
  
  hive -f create_access_logs.hql
  
  hive -f create_bleater_ids.hql

  hive -f sample_solution/create_bleats.hql

  hive -f sample_solution/create_movies.hql

  hive -f sample_solution/create_users.hql

  hive -e "CREATE TABLE bleatsfixed as SELECT * from bleats where get_json_object(bleat, '$.source') = 'Cloudera Movies';"

  hive -f join_data_sessions.hql
  
  cp -f sample_solution/json_sessions.py json_sessions.py
  hive -f join_data_json_sessions.hql
}

# Advance to Lab 4
lab4() {
  cd ~/training_materials/data_science/exercises/lab4_analysis/

  hive -f bleated_rating_counts.hql

  hadoop fs -getmerge \
        /user/hive/warehouse/bleated_rating_counts \
        bleat_counts.csv

  hive -f word_rating_counts.hql

  hadoop fs -getmerge \
        /user/hive/warehouse/word_rating_counts \
        word_counts.tsv

  rm -f word_scores.tsv

  # Need to create the word_scores.tsv file by executing the R 
  # files, but we don't want to do the interactive plotting 
  # from a script, so we filter that out
  egrep -v '^barplot' sample_solution/analyze_bleated_counts.R > awc_temp.R
  egrep -v '^plot' sample_solution/analyze_word_counts.R >> awc_temp.R

  # Now remove the reference to the external source file, since 
  # we're including it inline.
  egrep -v '^source' awc_temp.R > tmp.R

  # but we need to add this source reference back in
  echo 'source("word_functions.R")' > top.R
  cat top.R tmp.R > awc_temp.R
  rm -f top.R tmp.R

  R --no-save < awc_temp.R
            
  rm -f awc_temp.R
} 

# Advance to Lab 5
lab5() {
  # No-op placeholder.  This lab is self-contained; it does
  # not rely on data from any previous lab nor does any
  # subsequent lab rely on data from it.
  cd ~/training_materials/data_science/exercises/lab5_basicrecommender/
} 

# Advance to Lab 6
lab6() {
  cd ~/training_materials/data_science/exercises/lab6_buildrecs/

  hive -f step1_capture_ratings_to_csv.hql

  ./step2_run_mahout_baseline_recommender.sh

  hive -f step3_generate_ratings_with_bayes.hql

  ./step4_run_mahout_alternate_recommender.sh
} 

case "$1" in
        cleanup)
            cleanup
            ;;

        lab0)
            cleanup
            ;;

        lab1)
            cleanup
            lab0
            lab1
            ;;
         
        lab2)
            cleanup
            lab0
            lab1
            lab2
            ;;
            
        lab3)
            cleanup
            lab0
            lab1
            lab2
            lab3
            ;;         

        lab4)
            cleanup
            lab0
            lab1
            lab2
            lab3
            lab4
            ;;         

        lab5)
            cleanup
            lab0
            lab1
            lab2
            lab3
            lab4
            lab5
            ;;         

        lab6)
            cleanup
            lab0
            lab1
            lab2
            lab3
            lab4
            lab5
            lab6
            ;;         

        *)
            echo $"Usage: $0 {lab0|lab1|lab2|lab3|lab4|lab5|lab6}"
            exit 1
 
esac

