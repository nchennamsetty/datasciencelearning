#!/bin/bash

# Simple script to back up the contents of the data directory, so that a 
# student could later restore it if he or she accidentally modified the 
# original contents. It also backs up the access.log file in HDFS.

BACKUP_DIR=/var/tmp/dscdatabackup

# The DSDIR variable may not yet be defined if this
# script is invoked during initial bootup
DSDIR=/home/training/training_materials/data_science

if [ -d ${BACKUP_DIR} ]; then
    echo ""
    echo "ERROR: Previous backup already exists at ${BACKUP_DIR}."
    echo "       You must first rename or remove that directory before"
    echo "       using this script to create a new backup."
    echo ""
    exit 1
fi

mkdir -p ${BACKUP_DIR}
cp -R ${DSDIR}/data/* ${BACKUP_DIR}
hadoop fs -get /clouderamovies/access.log ${BACKUP_DIR}/access.log
gzip -9 ${BACKUP_DIR}/access.log

echo "Finished backup at `date`" > ${BACKUP_DIR}/.backup.info
