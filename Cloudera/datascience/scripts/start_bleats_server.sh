#!/bin/bash

echo "Starting Bleats server..."

# exported so the "stop" script can access the path easily
export BLEATS_RAW=/tmp/bleats_raw.json

# workaround for a bug in which the VM sometimes seems to 
# randomly unpack the bleats.json.gz file.
if [ -e $DSDIR/data/bleats.jzon.gz ]; then
    /usr/bin/gunzip -c $DSDIR/data/bleats.json.gz > $BLEATS_RAW
elif [ -e $DSDIR/data/bleats.json ]; then
    /bin/cp -f $DSDIR/data/bleats.json $BLEATS_RAW
else
   echo ""
   echo " *************************************"
   echo " ** ERROR: Bleats data unavailable! **"
   echo " *************************************"
   echo ""
   exit 1
fi

$DSDIR/scripts/social_server.py --bleats $BLEATS_RAW  > /dev/null 2>&1 &

# it takes a while for the server to start up, so we'll add
# an extra pause and give some feedback afterwards.
sleep 5
echo "Bleats server ready!"
