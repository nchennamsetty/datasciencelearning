#!/bin/bash

LAB_DIR=$DSDIR/exercises/lab6_buildrecs
DATA_DIR=$DSDIR/data

MOVIES=$DATA_DIR/u.item
ITEM_A=$LAB_DIR/itemrecs_a.txt
ITEM_B=$LAB_DIR/itemrecs_b.txt

if [ ! -e "$MOVIES" ]; then
    echo "ERROR: Movie file not found at $MOVIES"
    exit 1
fi
if [ ! -e "$ITEM_A" ]; then
    echo "ERROR: Item recommendation file not found at $ITEM_A"
    exit 2
fi
if [ ! -e "$ITEM_B" ]; then
    echo "ERROR: Item recommendation file not found at $ITEM_B"
    exit 3
fi


$DSDIR/scripts/diff_server.py \
  --movies $MOVIES \
  --recs1 $ITEM_A \
  --recs2 $ITEM_B > /dev/null 2>&1 &
