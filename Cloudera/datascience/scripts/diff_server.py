#! /usr/bin/env python

# ./diff_server.py --movies ~/cloudera/DS_course/webserver/ml-100k/u.item --recs1 ~/cloudera/DS_course/webserver/itemrecs_a25.txt --recs2 ~/cloudera/DS_course/webserver/itemrecs_b25.txt

import optparse
from bottle import Bottle, route, run, ConfigDict, response

def load_recs(filename):
  recs = {}
  with open(filename, 'r') as ip:
    for line in ip:
      ratings = []
      (uid, raw_ratings) = line.split('\t')
      for rating in raw_ratings.strip().strip('[]').split(','):
        (mid, r) = rating.split(':')
        ratings.append(mid)
      recs[uid] = ratings
  return recs

def weighted_correl(ratings1, ratings2):
  w1 = dict([(x[1], 1.0/(x[0] + 1)) for x in enumerate(ratings1)])
  w2 = dict([(x[1], 1.0/(x[0] + 1)) for x in enumerate(ratings2)])
  score = 0.0
  for mid in set(ratings1) | set(ratings2):
    score += abs(w1.get(mid, 0.0) - w2.get(mid, 0.0))
  return score

def jaccard_correl(ratings1, ratings2):
  numer = len(set(ratings1) & set(ratings2))
  denom = len(set(ratings1) | set(ratings2))
  return float(numer) / denom

# get server info and data files
argparser = optparse.OptionParser()
argparser.add_option('--host', default='localhost')
argparser.add_option('--port', type='int', default=9000)
argparser.add_option('--movies')
argparser.add_option('--recs1')
argparser.add_option('--recs2')
(opts, args) = argparser.parse_args()

# load name mapping
id2title = {}
with open(opts.movies, 'r') as ip:
  for line in ip:
    fields = line.split('|')
    id2title[fields[0].strip()] = fields[1].strip()

# load recommendation data
recs1 = load_recs(opts.recs1)
recs2 = load_recs(opts.recs2)

# compute Jaccard indices for ratings
uids = list(set(recs1.keys()) & set(recs2.keys()))  # only UIDs that have recs from both algorithms
jaccards = [jaccard_correl(recs1[uid], recs2[uid]) for uid in uids]
weighted = [weighted_correl(recs1[uid], recs2[uid]) for uid in uids]
idx = [i[0] for i in sorted(enumerate(weighted), key=lambda x:x[1], reverse=True)]

# define web application
app = Bottle()

@app.route('/')
def index():
    # some boilerplate stuff for page
    html = """<html>
              <head>
              <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
              <style>
              body {font-family: 'Droid Sans', sans-serif;}
              a:hover {color: #000; text-decoration: underline;}
              td {padding-left:10px; padding-bottom:5px;}
              </style>
              </head>
              <body>\n"""
    
    # generate table of UIDs and metrics
    html += '<table>\n'
    html += '<tr><td>User ID</td><td><a href="http://en.wikipedia.org/wiki/Jaccard_index">Jaccard Index</a></td><td><a href="http://en.wikipedia.org/wiki/Spearman%27s_rank_correlation_coefficient">Position-Weighted Difference (Variation of Spearmann Rank Correlation)</a></td></tr>'
    for i in idx:
        html += '<tr><td><a href="/user/%s">%s</a></td><td>%.3f</td><td>%.3f</td></tr>\n' % (uids[i], uids[i], jaccards[i], weighted[i])
    html += '</table>\n'
    
    # wrap it up
    html += """</body>
               </html>\n"""
    
    return html

@app.route('/user/<uid>')
def user(uid):
    # some boilerplate stuff for page
    html = """<html>
              <head>
              <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
              <style>
              body {font-family: 'Droid Sans', sans-serif;}
              a:hover {color: #000; text-decoration: underline;}
              td {padding-left:10px; padding-bottom:5px;}
              </style>
              </head>
              <body>\n"""
    
    # generate table of chosen movies along with scores
    html += '<table>\n'
    html += '<tr><td>Movie</td><td>Position in 1</td><td>Position in 2</td></tr>'
    movies = list(set(recs1[uid]) | set(recs2[uid]))
    for mid in movies:
        r1, r2 = -1, -1
        if mid in recs1[uid]:
          r1 = recs1[uid].index(mid) + 1
        if mid in recs2[uid]:
          r2 = recs2[uid].index(mid) + 1
        if r1 != r2:
          html += '<tr><td><b>%s</b></td><td><b>%d</b></td><td><b>%d</b></td></tr>\n' % (id2title[mid], r1, r2)
        else:
          html += '<tr><td>%s</td><td>%d</td><td>%d</td></tr>\n' % (id2title[mid], r1, r2)
    html += '</table>\n'
    
    # wrap it up
    html += """</body>
               </html>\n"""
    return html

run(app, host=opts.host, port=opts.port)

