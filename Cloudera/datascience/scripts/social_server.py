#!/usr/bin/python

from bottle import request, route, run
from optparse import OptionParser
import datetime
import json
import random
import time

BLEATS = {}

@route("/bleats/:id")
def bleats(id):
  # Sleep for a small amount of time to simulate API limits
  time.sleep(0.05)
  
  if id in BLEATS:
    return json.dumps(BLEATS[id])
  else:
    return "Baa? Bleat user id " + id + " not found."

def main():
  parser = OptionParser("usage: %prog [options]")
  parser.add_option("--host", default="127.0.0.1", dest="host")
  parser.add_option("--port", default="8080", dest="port")
  parser.add_option("--bleats", help="Bleat data file", dest="bleats_file")
  (opts, args) = parser.parse_args()

  for line in open(opts.bleats_file):
    record = json.loads(line)
    user_id = str(record['user']['id'])
    if user_id not in BLEATS:
      BLEATS[user_id] = []
    BLEATS[user_id].append(record)

  for (user_id, bleats) in BLEATS.iteritems():
    bleats.sort(key=lambda x: x['created_at'])
    for b in bleats:
      dt = datetime.datetime.fromtimestamp(b['created_at'])
      b['created_at'] = dt.strftime("%a %d %b %H:%M:%S +0000 %Y")

  run(host=opts.host, port=int(opts.port))

if __name__ == "__main__":
  main()
