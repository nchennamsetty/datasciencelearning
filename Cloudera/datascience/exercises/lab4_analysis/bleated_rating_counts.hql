DROP TABLE BLEATED_RATING_COUNTS;

CREATE TABLE BLEATED_RATING_COUNTS (bleated int, rating int, count int)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

add file extract_ratings.py;

INSERT OVERWRITE TABLE BLEATED_RATING_COUNTS 
SELECT bleated, rating, count(*) cnt
FROM (SELECT TRANSFORM(json) USING 'extract_ratings.py' 
      AS bleated, rating
      ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
      FROM JSON_SESSIONS) extracted_ratings
GROUP BY bleated, rating;
