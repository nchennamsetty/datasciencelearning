#!/usr/bin/python

import json
import re
import sys
from nltk import tokenize

MOVIE_RATING_RE = re.compile("^GET /rate\?movie=(\d+)&rating=(\d) HTTP/1\.1$")

def GetMovieAndRating(request):
  m = MOVIE_RATING_RE.match(request)
  if m:
    return (int(m.group(1)), int(m.group(2)))
  else:
    return (None, None)

def GetMovieIdFromBleat(bleat):
  if 'entities' in bleat:
    for link in bleat['entities']['urls']:
      return int((link['url'].split("/"))[-1])
  return None

def ParseBleatText(text):
  return [w for w in tokenize.wordpunct_tokenize(text.lower()) if len(w) > 2]

for line in sys.stdin:
  user_data = json.loads(line.strip())

  # First, pull out the user's ratings
  movie_ratings = {}
  for request in user_data['requests']:
    movie_id, rating = GetMovieAndRating(request)
    if movie_id:
      movie_ratings[movie_id] = rating

  # Then look for movies we have ratings for in the bleats.
  for bleat in user_data['bleats']:
    movie_id = GetMovieIdFromBleat(bleat)
    if movie_id in movie_ratings:
      five_stars = 0
      if movie_ratings[movie_id] == 5:
        five_stars = 1
      for token in set(ParseBleatText(bleat['text'])):
        print "%s\t%d" % (token, five_stars)
