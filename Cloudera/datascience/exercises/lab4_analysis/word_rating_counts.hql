DROP TABLE WORD_RATING_COUNTS;

CREATE TABLE WORD_RATING_COUNTS (word string, rating int, count int)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t';

add file extract_words_and_ratings.py;

INSERT OVERWRITE TABLE WORD_RATING_COUNTS 
SELECT word, rating, count(*) cnt
FROM (SELECT TRANSFORM(json) USING 'extract_words_and_ratings.py' 
      AS word, rating
      FROM JSON_SESSIONS) extracted_word_ratings
GROUP BY word, rating;
