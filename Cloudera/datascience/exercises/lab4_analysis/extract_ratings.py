#!/usr/bin/python

import json
import re
import sys

MOVIE_RATING_RE = re.compile("^GET /rate\?movie=(\d+)&rating=(\d) HTTP/1\.1$")

def GetMovieAndRating(request):
  m = MOVIE_RATING_RE.match(request)
  if m:
    return (int(m.group(1)), int(m.group(2)))
  else:
    return (None, None)

def GetMovieIdFromBleat(bleat):
  if 'entities' in bleat:
    for link in bleat['entities']['urls']:
      return int((link['url'].split("/"))[-1])
  return None

for line in sys.stdin:
  user_data = json.loads(line.strip())

  bleated_movies = set()
  for bleat in user_data['bleats']:
    movie_id = GetMovieIdFromBleat(bleat)
    if movie_id:
      bleated_movies.add(movie_id)

  for request in user_data['requests']:
    movie_id, rating = GetMovieAndRating(request)
    if movie_id:
      if movie_id in bleated_movies:
        print "1,%d" % rating
      else:
        print "0,%d" % rating
