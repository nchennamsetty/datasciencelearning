DROP TABLE MAHOUT_INPUT_A;

-- capture the information as a CSV
CREATE TABLE MAHOUT_INPUT_A
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
AS
SELECT cookie as user,
regexp_extract(request, "GET /rate\\?movie=(\\d+)&rating=(\\d) HTTP/1.1", 1) as movie,
CAST(regexp_extract(request, "GET /rate\\?movie=(\\d+)&rating=(\\d) HTTP/1.1", 2) as double) as rating
from ACCESS_LOGS
WHERE regexp_extract(request, "GET /rate\\?movie=(\\d+)&rating=(\\d) HTTP/1.1", 2) != "";
