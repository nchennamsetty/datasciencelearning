DROP TABLE MAHOUT_INPUT_B;

-- Create a table we can use to store the ratings for the
-- improved recommendations
CREATE TABLE MAHOUT_INPUT_B (user int, movie int, rating double)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

-- The next Hive query that will use the Naive Bayes model to 
-- create ratings for movies that users liked

-- Add the python script that will compute the ratings:
add file generate_ratings.py;

-- And a TSV file containing data similar to the semantic
-- analysis we did in lab #4
add file word_scores.tsv;

-- Now run a map-only Hive query that uses the ratings script 
-- to generate the ratings from the bleats and the log files.

INSERT OVERWRITE TABLE MAHOUT_INPUT_B 
SELECT user, movie, rating
FROM (SELECT TRANSFORM(json) 
      USING 'generate_ratings.py' 
      AS user, movie, rating
      ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
      FROM JSON_SESSIONS) generated_ratings;
