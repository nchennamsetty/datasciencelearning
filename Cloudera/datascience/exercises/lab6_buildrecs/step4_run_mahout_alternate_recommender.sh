#!/bin/sh

# STEP 4, From the command line:
# First, we need to remove that temp directory that Mahout created during our
# first run:
hadoop fs -rm -r -f temp

# Now, we kick off a new Mahout run using the new data we generated.
mahout recommenditembased --input /user/hive/warehouse/mahout_input_b \
  --output itemrecs_b -s SIMILARITY_PEARSON_CORRELATION -n 25

# Retrieve the recommendations for viewing.
hadoop fs -getmerge itemrecs_b itemrecs_b.txt
