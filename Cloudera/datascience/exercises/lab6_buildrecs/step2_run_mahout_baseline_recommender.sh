#!/bin/sh

# STEP 2, From the command line:
# We run Mahout's item recommender to generate 25 recommendations for each user,
# using the Pearson correlation similarity metric.
mahout recommenditembased --input /user/hive/warehouse/mahout_input_a \
  --output itemrecs_a -s SIMILARITY_PEARSON_CORRELATION -n 25

# We then retrieve the recommendations from Hadoop to evaluate them.
hadoop fs -getmerge itemrecs_a itemrecs_a.txt
