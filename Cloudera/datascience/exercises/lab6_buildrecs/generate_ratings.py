#!/usr/bin/python

import csv
import json
import math
import re
import sys
from nltk import tokenize

MOVIE_RATING_RE = re.compile("^GET /rate\?movie=(\d+)&rating=(\d) HTTP/1\.1$")

def GetMovieAndRating(request):
  m = MOVIE_RATING_RE.match(request)
  if m:
    return (int(m.group(1)), int(m.group(2)))
  else:
    return (None, None)

def GetMovieIdFromBleat(bleat):
  if 'entities' in bleat:
    for link in bleat['entities']['urls']:
      return int((link['url'].split("/"))[-1])
  return None

def ParseBleatText(text):
  return [w for w in tokenize.wordpunct_tokenize(text.lower()) if len(w) > 2]

def LoadWordScores(file, mi_threshold = 0.01):
  scores = {}
  header = True
  for row in csv.reader(open(file), delimiter="\t"):
    if header:
      # Skip header line.
      header = False
    else:
      word, cnt0, cnt1, mi = row[0], float(row[1]), float(row[2]), float(row[3])
      if mi > mi_threshold:
        cnt0, cnt1 = max(0.5, cnt0), max(0.5, cnt1)
        scores[word] = cnt1 / (cnt0 + cnt1)
  return scores

# Load the word scores we'll need for the Naive Bayes classifier.
PRIOR_PROB = float(7358) / float(11569)
WORD_SCORES = LoadWordScores("word_scores.tsv")

def IsProbablyFiveStar(bleat_text):
  # Log estimates are more stable than multiplying floating point numbers.
  pos_score = math.log(PRIOR_PROB)
  neg_score = math.log(1.0 - PRIOR_PROB)
  for token in set(ParseBleatText(bleat_text)):
    if token in WORD_SCORES:
      pos_score += math.log(WORD_SCORES[token])
      neg_score += math.log(1.0 - WORD_SCORES[token])
  return pos_score > neg_score

for line in sys.stdin:
  user_data = json.loads(line.strip())
  user_id = int(user_data['id'])

  # Get the ratings for movies the user explicitly rated.
  rated_movie_ids = set()
  for request in user_data['requests']:
    movie_id, rating = GetMovieAndRating(request)
    if movie_id:
      rated_movie_ids.add(movie_id)
      print "%d,%d,%d" % (user_id, movie_id, rating)

  # Then look for movies that were bleated, but not rated, and
  # see if we can infer a 5-star rating from the text.
  for bleat in user_data['bleats']:
    movie_id = GetMovieIdFromBleat(bleat)
    if movie_id not in rated_movie_ids and IsProbablyFiveStar(bleat['text']):
      print "%d,%d,5.0" % (user_id, movie_id)
