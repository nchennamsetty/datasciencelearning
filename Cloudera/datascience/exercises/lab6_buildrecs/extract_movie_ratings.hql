SELECT cookie as user,
regexp_extract(request, "GET /rate\\?movie=(\\d+)&rating=(\\d) HTTP/1.1", 1) as movie,
CAST(regexp_extract(request, "GET /rate\\?movie=(\\d+)&rating=(\\d) HTTP/1.1", 2) as double) as rating
from ACCESS_LOGS
WHERE regexp_extract(request, "GET /rate\\?movie=(\\d+)&rating=(\\d) HTTP/1.1", 2) != "";

