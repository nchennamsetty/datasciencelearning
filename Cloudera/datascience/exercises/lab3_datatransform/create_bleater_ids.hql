DROP TABLE BLEATER_IDS;

CREATE TABLE BLEATER_IDS
(
  user_id int,
  bleater_id int
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ',';

LOAD DATA LOCAL
INPATH '/home/training/training_materials/data_science/data/bleater_ids.csv'
OVERWRITE INTO TABLE BLEATER_IDS;
