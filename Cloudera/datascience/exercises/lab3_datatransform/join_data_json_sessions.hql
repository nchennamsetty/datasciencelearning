-- We'll make use of a transformation written in this Python file
add file json_sessions.py;

-- And convert that data into JSON.
DROP TABLE JSON_SESSIONS;

CREATE TABLE JSON_SESSIONS (json string);

INSERT OVERWRITE TABLE JSON_SESSIONS
SELECT json
FROM (SELECT TRANSFORM(id, age, gender, zipcode, requests, bleats)
      ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
      COLLECTION ITEMS TERMINATED BY '|'
      USING 'json_sessions.py'
      AS json
      FROM SESSIONS) json_output;
