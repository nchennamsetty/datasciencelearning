DROP TABLE USERS;

CREATE EXTERNAL TABLE USERS (
  id int,
  gender string,
  age int,
  occupation int,
  zipcode string
 
) 

ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION '/clouderamovies/userfixed';
