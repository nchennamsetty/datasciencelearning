DROP TABLE MOVIES;

-- Load the movie data.
CREATE EXTERNAL TABLE MOVIES
(
  id int,
  name string,
  year int
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION '/clouderamovies/moviefixed';
