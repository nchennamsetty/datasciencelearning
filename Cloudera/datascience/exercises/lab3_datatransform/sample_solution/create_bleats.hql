DROP TABLE BLEATS;

-- Load the bleats data. 
CREATE EXTERNAL TABLE BLEATS 
(
  bleat string
)
LOCATION '/clouderamovies/bleats';
