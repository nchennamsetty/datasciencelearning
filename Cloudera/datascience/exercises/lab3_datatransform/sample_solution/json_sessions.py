#!/usr/bin/python

import sys
import json

for line in sys.stdin:
  fields = line.strip().split('\t')
  
  # Handle missing data from outer joins and do other validation here
  if fields[0] == "\N":
    continue
	
  if fields[1] == "\N":
    fields[1] = 0
	
  # build a data structure and output it as JSON
  user_data = {
    'id': int(fields[0]),
    'age': int(fields[1]),
    'gender': fields[2],
    'zipcode': fields[3],
  }

  user_data['requests'] = fields[4].split("|")

  # Check to see if we have bleat data for this user.
  if len(fields) == 6:
    user_data['bleats'] = [json.loads(x) for x in fields[5].split("|")]
  else:
    user_data['bleats'] = []

  print json.dumps(user_data)

