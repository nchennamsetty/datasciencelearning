#!/usr/bin/env python

# TODO (A): Examine the ratings_data.py file.  This contains
# information about users and the ratings they've given to 
# several movies.

# The next line imports that rating information (which is a
# Python dictionary named 'customers') so that we can access
# it from our code.
from ratings_data import customers


# TODO (B): Implement a function to calculate the Euclidean 
# distance for two users, given the ratings that both gave
# to the same movies.

def calc_euclidean_distance(user_1_ratings, user_2_ratings):
     # Place your code here
     return distance


# TODO (C): Calculate the Euclidean distance that each user
# has with Alex, then print those values. Remember that
# smaller numbers indicate that these users share similar 
# preferences, while larger numbers mean that they have
# less in common.  Be sure to write these numbers down, 
# as they'll be used in the next part of this exercise.



# After completing this part of the exercise, you should
# be able to identify which user shares the most similar
# taste to Alex.  Who is this person?  


# There's another user who has very similar scores, but
# this is misleading because that person only has one
# movie in common with Alex.  Who is this?  

# TODO (D): After you identify the person who has only
# seen one of the same movies as Alex, go back and modify
# your program so that it will exclude that person from
# the distance calculations.
# TODO (E): Edit the ratings_data.py file and populate
# the 'similarities' dictionary so that it contains the
# similarity scores you've just calculated.  Don't forget
# to delete the entry corresponding to the user you've
# excluded in step D.
