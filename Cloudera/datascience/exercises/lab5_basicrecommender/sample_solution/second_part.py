#!/usr/bin/env python

from ratings_data import customers, similarities

# This function will be used to convert the Euclidean
# distance value passed as an argument into a value
# between 0 and 1 that we can use as a weighting factor.
# This will allow us to put a given user's ratings into 
# perspective for Alex, based on how that user's taste 
# match his own.
def calc_weighting_factor(euclidean_distance):
    val = float(euclidean_distance)
    if val == 0.0: return 1
    return 1 / val


# TODO (E): Calculate the weighted value for each
# user by passing that user's Euclidean distance
# to the function above.
# 
# Next, iterate over each movie that the user has
# seen (excluding those already seen by Alex) and
# multiply that user's rating of the movie by the
# weight to determine the weighted rating.  Once 
# you have calculated all weighted ratings for each
# movie, divide the sum of those weighted ratings
# by their count to produce an average for each film.
# Afterwards, sort these in descending numeric order
# so that the best suggestions for Alex are at the 
# top of the list.  Which two movies do you predict
# he'll like best?

user_names = similarities.keys()
considered = {}

for user_name in user_names:
   user_dist = similarities[user_name]
   weight = calc_weighting_factor(user_dist)

   for movie in customers[user_name]:
      # exclude movies that Alex has already seen
      if movie not in customers['Alex']:
         orig_rating = customers[user_name][movie]
         weighted_rating = float(orig_rating) * weight

         # initialize the dictionary
         if not movie in considered:
            considered[movie] = []

         considered[movie].append(weighted_rating)
    
suggestions = []
for movie in considered:
    total = 0.0
    count = 0

    for rating in considered[movie]:
        total = total + rating
        count = count + 1
    avg = total / count

    tuple = (avg, movie)
    suggestions.append( tuple )
  
suggestions.sort()
suggestions.reverse()

for movie in suggestions:  
    print "%f\t%s" % (movie[0], movie[1])
