#!/usr/bin/env python

# TODO (A): Examine the ratings_data.py file.  This contains
# information about users and the ratings they've given to 
# several movies.

# The next line imports that rating information (which is a
# Python dictionary named 'customers') so that we can access
# it from our code.
from ratings_data import customers
from math import sqrt


# TODO (B): Implement a function to calculate the Euclidean 
# distance for two users, given the ratings that both gave
# to the same movies.

def calc_euclidean_distance(user_1_ratings, user_2_ratings):
     distance = 0.0
     for i in range(len(user_1_ratings)):
         rate_a = user_1_ratings[i]
         rate_b = user_2_ratings[i]

         distance = distance + pow((rate_a - rate_b), 2)
     return sqrt(distance)


# TODO (C): Calculate the Euclidean distance that each user
# has with Alex, then print those values. Remember that
# smaller numbers indicate that these users share similar 
# preferences, while larger numbers mean that they have
# less in common.  Be sure to write these numbers down, 
# as they'll be used in the next part of this exercise.

user_names = customers.keys()

for user_name in user_names:
   # Don't compare Alex to himself
   if user_name == "Alex":
      continue
   
   # since dictionaries don't guarantee order, we'll use lists
   # to hold our ratings so that the order is consistent
   alex_ratings = []
   other_ratings = []

   # Check each movie that this user has in common with Alex
   for movie in customers['Alex']:
      if movie in customers[user_name]:
         alex_ratings.append(customers['Alex'][movie])
         other_ratings.append(customers[user_name][movie])

   # (SEE D BELOW) the 'if' statement here excludes the misleading result
   if len(other_ratings) >= 1:
      dist = calc_euclidean_distance(alex_ratings, other_ratings)
      print "Distance for  %s is %f" % (user_name, dist)

# After completing this part of the exercise, you should
# be able to identify which user shares the most similar
# taste to Alex.  Who is this person?  
# 
# There's another user who has very similar scores, but
# this is misleading because that person only has one
# movie in common with Alex.  Who is this?  

# TODO (D): After you identify the person who has only
# seen one of the same movies as Alex, go back and modify
# your program so that it will exclude that person from
# the distance calculations.
# TODO (E): Edit the ratings_data.py file and populate
# the 'similarities' dictionary so that it contains the
# similarity scores you've just calculated.  Don't forget
# to delete the entry corresponding to the user you've
# excluded in step D.
