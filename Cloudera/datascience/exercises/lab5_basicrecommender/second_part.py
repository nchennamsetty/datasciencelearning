#!/usr/bin/env python

from ratings_data import customers, similarities

# This function will be used to convert the Euclidean
# distance value passed as an argument into a value
# between 0 and 1 that we can use as a weighting factor.
# This will allow us to put a given user's ratings into 
# perspective for Alex, based on how that user's taste 
# match his own.
def calc_weighting_factor(euclidean_distance):
    val = float(euclidean_distance)
    if val == 0.0: return 1
    return 1 / val


# TODO (E): Calculate the weighted value for each
# user by passing that user's Euclidean distance
# to the function above. Next, iterate over each 
# movie that the user has seen (excluding those 
# already seen by Alex) and multiply that user's 
# rating of the movie by the weight to determine 
# the weighted rating.  Once you have calculated 
# all weighted ratings for each movie, divide the 
# sum of those weighted ratings by their count to 
# produce an average for each film. Afterwards, 
# sort these in descending numeric order so that 
# the best suggestions for Alex are at the top of 
# the list.  Which two movies do you predict he'll 
# like best?
