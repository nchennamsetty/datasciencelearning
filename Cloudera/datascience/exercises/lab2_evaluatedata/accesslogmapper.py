#!/usr/bin/python

import datetime
import re
import sys

# For extracting the date and request info from the request.
pattern = re.compile(r"^.*\[(.*)\] \"GET (.*) HTTP/1.1\".*$")

def StandardizeDate(date_str):
  (dt_str, tz) = date_str.split(" ")
  dt = datetime.datetime.strptime(dt_str, "%d/%b/%Y:%H:%M:%S")
  if tz == "-0500":  # East Coast
    dt += datetime.timedelta(hours=5)
  else: # Pacific
    dt += datetime.timedelta(hours=8)
  return dt.strftime("%d/%b/%Y:%H:%M:%S")

_FIX_RATINGS = {
  "10": "2",
  "11": "3",
  "100": "4",
  "101": "5"
}

def FixRatings(request):
  if "/rate?" in request:
    _, request_args = request.split("?")
    movie, rating_str = request_args.split("&")
    _, rating = rating_str.split("=")
    if rating in _FIX_RATINGS:
      return "/rate?%s&rating=%s" % (movie, _FIX_RATINGS[rating])
  return request

for line in sys.stdin:
  line = line.strip()
  res = pattern.match(line)
  if res:
    date_str = res.group(1)
    line = line.replace(date_str, StandardizeDate(date_str))
    request = res.group(2)
    line = line.replace(request, FixRatings(request))
  print line
