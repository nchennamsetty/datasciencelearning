#!/usr/bin/python

import sys
import json

for line in sys.stdin:
  user, movies, mixer = line.strip().split("\t")

  # Parse the mixer data as a JSON string.
  mixer_data = json.loads(mixer)
  inter = mixer_data['i']
  # Parse the movies into the movie ids.
  selected_ids = [int(x) for x in json.loads(movies)]

  # Find the index of the lowest-ranked movie that was
  # selected by the user.
  cmax = max([inter.index(x) for x in selected_ids if x in inter])
  cmovie = inter[cmax]
 
  # Use the position of cmovie in 'a' and 'b' to determine
  # 'k', the smallest value such that the union of a[0:k]
  # and b[0:k] contains all of the movies that were selected.
  a = mixer_data['a']
  b = mixer_data['b']
  if cmovie in a:
    k = a.index(cmovie)
    if cmovie in b:
      k = min(k, b.index(cmovie))
  else:
    k = b.index(cmovie)
  
  # Determine which of the subranges of a and b contains
  # more of the movies the user selected.
  selected_set = set(selected_ids)
  ha = len(selected_set.intersection(set(a[0:k])))
  hb = len(selected_set.intersection(set(b[0:k])))
  if ha == hb:
    print "tie"
  elif ha > hb:
    print "a"
  else:
    print "b"
