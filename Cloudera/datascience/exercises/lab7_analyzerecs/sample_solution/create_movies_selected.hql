DROP TABLE movies_selected;

-- Filter the recs logs to just be the user ID and the movie 
-- they added, and then group them by user id so that we have 
-- all of the movies a user added in a single row.
CREATE TABLE MOVIES_SELECTED
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
COLLECTION ITEMS TERMINATED BY '|'
AS
SELECT cookie as user,
collect_set(regexp_extract(request, "GET /addMovie\\?id=(\\d+) HTTP/1.1", 1)) as movies
FROM RECS_LOGS
WHERE regexp_extract(request, "GET /addMovie\\?id=(\\d+) HTTP/1.1", 1) != ""
GROUP BY cookie;

