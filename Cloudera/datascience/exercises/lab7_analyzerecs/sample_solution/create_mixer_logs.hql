DROP TABLE mixer_logs;

-- Load the mixer log JSON records.
CREATE EXTERNAL TABLE MIXER_LOGS (
  entry string
) 
LOCATION '/clouderamovies/mixerlogs';
