#!/usr/bin/python3 -tt
# Copyright 2013 Naren .
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0


"""Hidden Markov exercise

Exercise from Think python book

Steps:
-read file and construct markov dictionary
  
- for a given prefix length p, identify suffix s

For e.g. "A tale of two cities has been told for long and long time"
{A tale: [of], tale of: [two], of two:[cities, birds] }

"""
# TODO - replace string key with tuple
import sys
import re
import random

def generate_markov_dict(filename, prefix_len = 2):
	file = open(filename, 'r')
	#print words
	dict= {}
	
	words = file.read().split()
	#print words
	#words = clean_words(words)
	for index, v in enumerate(words):
		prefix_key = ''
		# getting prefix
		if index < len(words)- prefix_len:
			for i in range(prefix_len):
				prefix_key += words[index + i] + ' '
			prefix_key = prefix_key.strip()
		   # determine suffix 
			suffix_indx = index + prefix_len 
			if suffix_indx < len(words):
			  suffix = words[suffix_indx]
			else:
			  suffix=''
		   # add prefix/suffix to dict
			if not prefix_key in dict.keys():
			  dict[prefix_key] = [suffix]
			else:
			  dict[prefix_key].append(suffix)
	file.close()
	dictfile = open('dict.txt' , 'w')
	for item in dict.items():
		dictfile.write(item[0]+' : '+ str(item[1]) + '\n')
	dictfile.close()
	#print(dict.keys())
	return dict  
 


    
def print_markov_text(filename, linecount = 10, prefix_len = 4):
    
    markov_dict = generate_markov_dict(filename, prefix_len)
   
    rand_key = random.choice(list(markov_dict.keys()))
    sentence= rand_key.capitalize() + ' '+ random.choice(markov_dict[rand_key])
    #print sentence
    
    
    for i in range(linecount):
        words = sentence.split() #can we just use a tuple here ??
        #print words
        new_prefix, newsuffix ='', ''
        for word in words[len(words)- prefix_len:]:
            new_prefix += word + ' ' 
        new_prefix = new_prefix.strip()
        
        newsuffix = random.choice(markov_dict[new_prefix])    
        sentence += ' ' + newsuffix
        #print('new_suffix: ' + newsuffix)
   
    print(sentence)
        
        



           
        
 
 
def clean_words(words):
    return [re.sub('\W', '', word).strip().lower() for word in words]       
        


def main():
  if not len(sys.argv) in [2, 4, 6]:
    print ('usage: ./hiddenmarkov.py file {--print numoflines} {--prefix nr}')
    sys.exit(1)

  
  filename = sys.argv[1]
  print_markov_text(filename, 400, 2)
  
  


if __name__ == '__main__':
  main()

