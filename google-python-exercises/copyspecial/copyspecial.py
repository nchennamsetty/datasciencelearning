#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""

# +++your code here+++
# Write functions and modify main() to call them
def list_special_files(dir):
   specialfiles = []
   if os.path.exists(dir):
      for filename in os.listdir(dir):
          match = re.search(r'[\w]+__[\w]+__.+', filename)
          if match:
             specialfiles.append(os.path.join(dir, filename))
   return specialfiles
   
   
def copy_special_files(srcdir, todir):
    specialfiles = list_special_files(srcdir)
    print specialfiles
    for file in specialfiles:
        if not os.path.exists(todir): os.mkdir(todir)
        shutil.copy(file, todir)
        
def zip_special_files(srcdir, tozip):
    specialfiles = list_special_files(srcdir)
    cmd = 'zip -j ' + tozip + ' '
    for file in specialfiles:
        cmd += os.path.abspath(file) + ' '
    #print cmd
    (status, output) = commands.getstatusoutput(cmd)
    print output
            

def main():
  # This basic command line argument parsing code is provided.
  # Add code to call your functions below.

  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]
  if not args:
    print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
    sys.exit(1)

  # todir and tozip are either set from command line
  # or left as the empty string.
  # The args array is left just containing the dirs.
  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    sdir = os.path.abspath(args[2])
   
    
    copy_special_files(sdir, todir)
    del args[0:2]

  tozip = ''
  if args[0] == '--tozip':
    tozip = args[1]
    sdir = os.path.abspath(args[2])
    zip_special_files(sdir, tozip)
    del args[0:2]
  

  if len(args) == 0:
    print "error: must specify one or more dirs"
    sys.exit(1)
  else:
    dir = args[0]
    list_special_files(dir)
    


  # +++your code here+++
  # Call your functions
  
if __name__ == "__main__":
  main()
