function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%


X = [ones(m, 1) X];
A2 = sigmoid(X*Theta1'); %A2 size is 5000,25
A2 = [ones(m, 1) A2]; % 5000, 26
h = sigmoid(A2*Theta2'); %h size 5000, 10

%intermediate cost functio
% getting Y in 5000x10 format

k_values = 1:num_labels; % k_ve
for i=1:size(y,1)
   
  %Y is 5000 x 10 in form of
  %[1 0 0 0 0..
  %0 1 0 0 ..]
  
  Yvec(i,:) = (y(i,:) == k_values) ;
end

%intermediate_cost func term
temp_Cost = (-Yvec.*log(h))-((1-Yvec).*log(1-h));

%regularization terms
%ignoring theta 1, 2 bias terms

th1 = Theta1(:, 2:size(Theta1,2));
th2 = Theta2(:, 2:size(Theta2,2));

reg_term = (0.5*lambda/m)*( sum(sum(th1.^2),2) + sum(sum(th2.^2),2));


J= (sum(sum(temp_Cost, 2))/m) + reg_term ;

% Back propagation algorithm
DELTA1 = zeros(size(Theta1_grad)); % 25 x 401
DELTA2 = zeros(size(Theta2_grad));
for t = 1:m
    x_t = X(t,:);
    z2_t = x_t*Theta1';
    a2_t = sigmoid(z2_t); % 1x25
    a2_t = [1 a2_t]; % 1x 26
    
    a3_t = sigmoid(a2_t*Theta2'); % 1 x 10
    y_tk = (y(t,:) == k_values) ; % 1 x 10
    
    small_delta3 = a3_t - y_tk; % 1 x 10
   % Theta 2 10x26
   % z2 1x25
    small_delta2 = (small_delta3*Theta2(:,2:end)).*sigmoidGradient(x_t*Theta1'); % 1x25
    
    
    DELTA1 = DELTA1 + (small_delta2'*x_t);
    DELTA2 = DELTA2 + (small_delta3'*a2_t);
    
    
    
    
end

r1 = Theta1;
r1(:,1)= 0;
r2 = Theta2;
r2(:,1) =0;

Theta1_grad = DELTA1/m + (1.0*lambda*r1/m) ;
Theta2_grad = DELTA2/m  + (1.0*lambda*r2/m);


















% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
