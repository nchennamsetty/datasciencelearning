#!/usr/bin/python
# Copyright Narendra Chennamsetty

""" WordNet digraph

the purpose of this program is to construct digraph
calculate distance between two nouns


"""




import networkx as nx
import matplotlib.pyplot as plt
import sys
import re

def  construct_wordnet(synsetsfile, hypernymfile):
    print(synsetsfile)
    sf = open(synsetsfile,'r')
    G = nx.DiGraph()

    for line in sf:
        fields = line.split(',')
        G.add_node(int(fields[0]))


    hf = open(hypernymfile,'r')
    for l in hf:
        fields = l.split(',')
        v = fields[0]
        
        for w in fields[1:]:
            G.add_edge(int(v),int(w.replace("\n", "")))

    sf.close()
    hf.close()
    return G

def main():

  
  synsetfile = "./data/synsets.txt"
  hypernymfile = "./data/hypernyms.txt"
  G =  construct_wordnet(synsetfile, hypernymfile)
  #print(G.edges())
  #  nx.draw(G.subgraph(range(1, 100)))
  # plt.draw()
  


if __name__ == '__main__':
  main()

