#!/usr/bin/python

import sys
recos = {}
for line in sys.stdin.readlines():
    try:
        (i, R) = line.strip().split('\t')
        key = i.split(',')[0]
        print(key+'\t'+R)

    except Exception, e:
       sys.stderr.write("reporter:counter:Rows,NoFriends,1\n")
