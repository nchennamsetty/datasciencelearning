#!/usr/bin/python
# Author Narendra Chennamsetty
# narench@gmail.com
# SUNET - narench@stanford.edu
# CS246 Homework 1 Q1
# FriendPairMapper

import os
import sys

# take 0 1,2,3,4 and output
# (0,1), (1,2,3,4)

for line in sys.stdin.readlines():
    
    try:
        fields = line.strip().split('\t')
        if len(fields) > 1:
            i, R = fields[0].strip(), fields[1].strip()
            for j in R.split(','):
                if i < j:
                    key = i +',' + j
                else:
                    key = j+','+i

                print('%s\t%s' %(key, i+','+ R))
                    
    except:
       sys.stderr.write("reporter:counter:Rows,NoFriends,1\n")
       

