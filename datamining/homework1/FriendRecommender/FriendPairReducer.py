#!/usr/bin/python
# Author Narendra Chennamsetty
# narench@gmail.com
# SUNET - narench@stanford.edu
# CS246 - Homework 1 Q1
#FriendPairReducer
import sys

# the input is going to be in format
# 0,1 0,1,3,4,5
# 0,1 1,3,4,5,10
# 0.10 
lines =  sys.stdin.readlines()
k = 0 
#R = friends of i
#S = friends of j
recomms = {}
while k< len(lines)-1:
      line1 = lines[k].strip()
      line2 = lines[k+1].strip()
      (key1, Rstr) = line1.strip().split('\t')
      (key2, Sstr) = line2.strip().split('\t')
      Rvals = Rstr.split(',')
      i = Rvals[0]
      R = set(Rvals[1:])
      Svals = Sstr.split(',')
      j = Svals[0]
      S = set(Svals[1:])
      
      i_Recos = (S-R)
      j_Recos = (R-S)

      if i in recomms.keys():
            recomms[i] = recomms[i]+ list(i_Recos)
            #print("i = {0}, Recos = {1}".format(i,i_Recos))
      else:
            recomms[i] = list(i_Recos)
      if j in recomms.keys():
            recomms[j] = recomms[j]+ list(j_Recos)
            #print("j = {0}, Recos = {1}".format(j,j_Recos))
      else:
            recomms[j] = list(j_Recos)
      
      k = k+2
for key in recomms.keys():
      recomm_dict = {}
      for item in recomms[key]:
           if item in recomm_dict.keys():
                 recomm_dict[item]+=1
           else:
                 recomm_dict[item] = 1
      sorted_recomm =[k for k,v in sorted(recomm_dict.items(), key= lambda x:x[1], reverse=True)]
      if key in sorted_recomm: sorted_recomm.remove(key)
      if len(sorted_recomm) > 10:
            ten_sorted_recomms = sorted_recomm[:10]
      else:
            ten_sorted_recomms = sorted_recomm
      print(key+'\t'+','.join(ten_sorted_recomms))
      
    
      
