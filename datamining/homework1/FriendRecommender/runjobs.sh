#!/bin/bash
int_out_dir = "/user/cloudera/outtest1"
hadoop fs -rm -R "$int_out_dir"
hadoop jar /usr/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.0.0-mr1-cdh4.4.0.jar \
-mapper FriendPairMapper.py \
-reducer FriendPairReducer.py \
-input /user/cloudera/testin.txt \
-output "$int_out_dir" \
-file FriendPairMapper.py \
-file FriendPairReducer.py

hadoop jar /usr/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.0.0-mr1-cdh4.4.0.jar \
-mapper RecommMapper.py \
-reducer RecommReducer.py \
-input "$int_out_dir" \
-output /user/cloudera/rec1 \
-file RecommMapper.py \
-file RecommReducer.py

40  cat soc-LiveJournalAdj.txt | python FriendPairMapper.py > m1.txt
41  sort -k1,2 m1.txt m1sorted.txt
42  sort -k1,2 m1.txt > m1sorted.txt
43  sort -k1 m1.txt > m1sorted.txt
44  cat soc-LiveJournalAdj.txt | python FriendPairMapper.py > m1.txt
45  sort -k1,2 m1.txt > m1sorted.txt
46  cat m1sorted.txt | python FriendPairReducer.py
47  cat m1sorted.txt | python FriendPairReducer.py > r1.txt
