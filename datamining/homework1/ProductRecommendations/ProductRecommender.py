#!/usr/bin/python
# Author 2014 Narendra Chennamsetty
# SUNET narench@stanford.edu
#CS246 HW 1 Q2
# implement A-priori algorithm to do the product recommendations

import sys
import itertools
import json


def main():
    """main function"""
    
    support = 100
    # first pass
    browsing_hist_file = 'browsing.txt'
    freq_products = getfreqproducts(browsing_hist_file, support)
    freq_prod_pairs = getfreqproductpairs(sorted(list(freq_products.keys())), browsing_hist_file)
    freq_prod_triples = getfreqproducttriples(sorted(list(freq_products.keys())), browsing_hist_file)
    conf_scores_pairs = getconfidencescoresofpairs(freq_products, freq_prod_pairs)

    for i in range(1,10):
        print("conf({0}->{1}) = {2}".format(conf_scores_pairs[i][0][0], conf_scores_pairs[i][0][1], conf_scores_pairs[i][1]))
    conf_scores_triples = get_confidence_scores_triples(freq_prod_pairs, freq_prod_triples)

    conf_trpls_sorted = sorted(conf_scores_triples.items(), key = lambda x:x[1], reverse = True)
    for i in range(1,100):
        print  ("conf({0},{1}->{2}) = {3}".format(conf_trpls_sorted[i][0][0][0],conf_trpls_sorted[i][0][0][1], conf_trpls_sorted[i][0][1], conf_trpls_sorted[i][1] ))
    

def getfreqproducts(browsing_hist_file, support):
    """This function does the first pass of A-priori algorithm and gets frequent products"""
    products = {}
    with open(browsing_hist_file) as f:
        for line in f:
            basket_products =sorted([w.strip() for w in line.split()])
            for i in basket_products:
                if i in products.keys():
                    products[i] = products[i]+1
                else:
                    products[i] = 1
    
    freq_products = {k:v for k,v in products.items() if v > support}
    return freq_products 


                    
def getfreqproductpairs(freq_products, browsing_hist_file):
    """This function does the second pass of A-priori alogorithm and gets frewquent product pairs"""
    freq_pairs = {}
    with open(browsing_hist_file) as f:
        for line in f:
            basket_products =sorted([w.strip() for w in line.split()])
            freq_prods_in_basket = [p for p in basket_products if p in freq_products]
            freq_pairs_in_basket = list(itertools.combinations(freq_prods_in_basket,2))
            if len(freq_pairs) == 0:
                freq_pairs[freq_pairs_in_basket[0]] = 0
            for fpb in freq_pairs_in_basket:
                if fpb in freq_pairs.keys():
                    freq_pairs[fpb] = freq_pairs[fpb]+1
                else:
                    freq_pairs[fpb] = 1
                    
    return freq_pairs

def getfreqproducttriples(freq_products, browsing_hist_file):
    """This function does the second pass of A-priori alogorithm and gets frewquent product pairs"""
    freq_triples = {}
    with open(browsing_hist_file) as f:
        for line in f:
            basket_products =sorted([w.strip() for w in line.split()])
            freq_prods_in_basket = [p for p in basket_products if p in freq_products]
            freq_triples_in_basket = list(itertools.combinations(freq_prods_in_basket,3))
            if len(freq_triples) == 0:
                freq_triples[freq_triples_in_basket[0]] = 0
            for fpb in freq_triples_in_basket:
                if fpb in freq_triples.keys():
                    freq_triples[fpb] = freq_triples[fpb] + 1
                else:
                    freq_triples[fpb] = 1
    saveobjtofile(repr(freq_triples), 'freq_triples.json')                
    return freq_triples

def getconfidencescoresofpairs(freq_prods, freq_product_pairs):
    """Gets the confidence score of all product pairs"""
    conf_scores = {}
    for k  in freq_product_pairs.keys():
        conf_scores[k] = freq_product_pairs[k]/freq_prods[k[0]]
        k_reverse = tuple(reversed(k))
        conf_scores[k_reverse] =freq_product_pairs[k]/freq_prods[k_reverse[0]]

    conf_scores_list = sorted(conf_scores.items(), key = lambda x: x[1], reverse= True)
    #saveobjtofile(conf_scores_list, 'confscores_pairs.json')
    return conf_scores_list



def get_confidence_scores_triples(freq_pairs, freq_triples):
    """Gets the confidence score of product triples"""
    # conf_score data structure for triples  is going to be  {((item1,item2), item3)) : 0.555}
    conf_scores_triples = {}
    for triple in freq_triples.keys():
        for pair in list(itertools.combinations(triple,2)):
            for k in triple:
                if k not in pair: 
                    e = k
                    break
            conf_scores_triples[(pair,e)] = freq_triples[triple]/freq_pairs[pair]
    #saveobjtofile(repr(conf_scores_triples), 'conftriples.json')
    return conf_scores_triples
            
    
def saveobjtofile(obj, filename):
    with open(filename, 'w') as f:
        json.dump(obj, f)

    

if __name__  == '__main__':
   main()
