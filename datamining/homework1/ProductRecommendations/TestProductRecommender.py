#!/usr/bin/python
""" Unit tests for Product Recommender"""
import ProductRecommender
import unittest

class TestProductRecommender(unittest.TestCase):
    """This is unit test for testing freqproducts"""

    def test_freq_prod_list(self):
        """should correctly identify the"""
        freq_products = ProductRecommender.getfreqproducts('testinput.txt', 1)
        self.assertEqual(11, freq_products['ELE17451'])
        self.assertEqual(5, freq_products['SNA69641'])

    def testConfidenceScoresPairs(self):
        """Test confidence scores for ELE17451 -> SNA69641"""
        freq_products = ProductRecommender.getfreqproducts('testinput.txt', 1)
        freq_prod_pairs = ProductRecommender.getfreqproductpairs(sorted(list(freq_products.keys())), 'testinput.txt')
        conf_scores = ProductRecommender.getconfidencescoresofpairs(freq_products, freq_prod_pairs)
        self.assertTrue(len(conf_scores)> 0)
        for k,v in conf_scores:
            if k == ('ELE17451', 'SNA69641'):
                #print(k[0] + "->" + k[1]+ ":" +str(v))
                self.assertEqual(v,4/11)
            if k == ('SNA69641', 'ELE17451'):
                self.assertEqual(v,4/5)
    def test_confidence_scores_triples(self):
        """tests confidence scores for triples"""
        freq_products = ProductRecommender.getfreqproducts('testinput.txt', 1)
        freq_prod_pairs = ProductRecommender.getfreqproductpairs(sorted(list(freq_products.keys())), 'testinput.txt')
        freq_prod_triples =ProductRecommender.getfreqproducttriples(sorted(list(freq_products.keys())), 'testinput.txt')
        conf_scores = ProductRecommender.get_confidence_scores_triples(freq_prod_pairs, freq_prod_triples)     
        self.assertTrue(len(conf_scores) >0)
        for k,v in conf_scores:
            if k == (('FRO11987', 'SNA90258'), 'GRO99222'):
                self.assertEqual(v, 1/2)
            if k == (('ELE17451', 'SNA69641'), 'FRO78087'):
                self.assertEqual(v, 2/3)
if __name__ == "__main__":
	unittest.main()
