% Homework 4 Plotting 10 near neighbors for image 100 found using LSH and 
% Linear search

clear all;

load patches;
img = patches(:,100);

nn =11;
L = 10;
k = 24;
nnlsh = 0;
T = lsh('lsh',L,k,size(patches,1),patches,'range',255);

while length(nnlsh) < nn
    [nnlsh,numcand]=lshlookup(img,patches,T,'k',nn,'distfun','lpnorm','distargs',{1});
end

figure(1);clf;
title('LSH Search');
for k=1:nn-1
    subplot(3,4,k);
    imagesc(reshape(patches(:,nnlsh(k+1)),20,20)); colormap gray;axis image;
    xlabel('Neighbor');
end
subplot(3,4,11);
imagesc(reshape(img,20,20)); colormap gray;axis image; xlabel('Original');


% ---Linear Search ---%
d=sum(abs(bsxfun(@minus,img,patches)));
[ignore,ind]=sort(d);

figure(2);clf;
title('Linear Search');
for k=1:nn-1
    subplot(3,4,k);
    imagesc(reshape(patches(:,d(k+1)),20,20)); colormap gray;axis image;
    xlabel('Neighbor');
end
subplot(3,4,11);
imagesc(reshape(img,20,20)); colormap gray;axis image; xlabel('Original');
