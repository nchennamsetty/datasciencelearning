clear all;
load patches;

L = 10;
k = 24;
nn = 4;
T = lsh('lsh',L,k,size(patches,1),patches,'range',255);
for j = 1:10
    img = patches(:,j*100);
    %LSH near neighbhor search%
    tic;
    [nnlsh,numcand]=lshlookup(img,patches,T,'k',nn,'distfun','lpnorm','distargs',{1});
    t_lsh(j) = toc;
    
    %Linear Search%
    tic;d=sum(abs(bsxfun(@minus,img,patches)));
    [ignore,ind]=sort(d);
    t_linsrch(j) = toc;
end
fprintf('Average LSH Search time %f', mean(t_lsh));
fprintf('Average Linear Search time %f', mean(t_linsrch));

%Plotting error
clear all;
load patches;

k = 24;
L = 10:2:20
nn = 4; 
nnlsh = 0; %Initial value

for idx = 1:length(L)
    T = lsh('lsh',L(idx),k,size(patches,1),patches,'range',255);
    for j = 1:10
        img = patches(:,j*100);
        % LSH near neighbhor search %
        while length(nnlsh) < nn
            [nnlsh,numcand]=lshlookup(img,patches,T,'k',nn,'distfun','lpnorm','distargs',{1});
        end
        
        %L1 norm distance from img to nearest neighbors
        for i = 1:nn-1
            dist_lsh(i,j) = sum(abs(img - patches(:,nnlsh(i+1))));
        end
        
        %Linear Search%
        d=sum(abs(bsxfun(@minus,img,patches)));
        [ignore,ind]=sort(d);
        for i = 1:nn-1
            dist_linear(i,j) = sum(abs(img - patches(:,ind(i+1))));
        end
        
    end

error(idx) = mean(sum(dist_lsh)./sum(dist_linear));
end

figure(1);
plot(L, error,'-x');
title('Error vs L');
xlabel('L');
ylabel('Error')

clear all;
load patches;
k = 16:2:24;
L = 10;
nn = 4; 
nnlsh = 0; %Initial value

for idx = 1:length(k)
    T = lsh('lsh',L,k(idx),size(patches,1),patches,'range',255);
    for j = 1:10
        img = patches(:,j*100);
        % LSH near neighbhor search %
        while length(nnlsh) < nn
            [nnlsh,numcand]=lshlookup(img,patches,T,'k',nn,'distfun','lpnorm','distargs',{1});
        end
        
        %L1 norm distance from img to nearest neighbors
        for i = 1:nn-1
            dist_lsh(i,j) = sum(abs(img - patches(:,nnlsh(i+1))));
            
        end
        
        %Linear Search%
        d=sum(abs(bsxfun(@minus,img,patches)));
        [ignore,ind]=sort(d);
        for i = 1:nn-1
            dist_linear(i,j) = sum(abs(img - patches(:,ind(i+1))));
            
        end
        
    end

error(idx) = mean(sum(dist_lsh)./sum(dist_linear));
end

figure(2);
plot(k, error,'-x');
title('Error vs k');
xlabel('k');
ylabel('Error')






