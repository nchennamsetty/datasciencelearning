#/usr/bin/python

import numpy as np


x = np.loadtxt('points.txt', delimiter=',')
y = np.loadtxt('classes.txt')

w = np.array([-1,1])
b = -2
slack = 0
errors = 0
for i in range(0, x.shape[0]):
    t = y[i]*(np.dot(w,x[i]) + b)
    if t < 0:
        errors+=1
    slack+= (1- t)
    print(" {}  {}  errors = {} slack = {} ".format(x[i],y[i], errors, slack))
    