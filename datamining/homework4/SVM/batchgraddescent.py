#!/usr/bin/python
#Author Narendra Chennamsetty
#SUNET narench@stanford.edu

import numpy as np
import time
C = 100
def main():
    x = np.loadtxt('HW4-q1/features.txt', delimiter=',')
    y = np.loadtxt('HW4-q1/target.txt')
    b = 0
    w_vec = np.zeros(x.shape[1])
    eta = 0.0000003
    
    k = 1
    delta = 10
    oldval = -1
    while delta > 0.25:
        
        wnew = w_vec - eta*derv_objfunc_w(w_vec,b,x,y)
        b = b - eta*derv_objfunc_b(w_vec,b,x,y)
        newval = objfunc(w_vec,b,x,y)
        print(newval)
        if k > 1:
            delta = abs(oldval - newval)*100/oldval
            
        k+=1
        w_vec = wnew
        oldval = newval
    print("Iterations = {}, value = {}".format(k,oldval))   
    

def objfunc(w_vec,b,x,y):
    total = 0
    for i in range(0, x.shape[0]):
        t = y[i]*(np.dot(w_vec,x[i]) + b)
        if t < 1:
            total += (1-t)
    return 0.5*sum(w_vec**2) + C*total
    

def derv_objfunc_w(w_vec,b,x,y):
    
    L_derv_w = np.zeros(w_vec.shape)
    for i in range(0, x.shape[0]):
        if y[i]*(np.dot(w_vec,x[i]) + b) < 1:
                 L_derv_w = L_derv_w -y[i]*x[i]
    return w_vec +C* L_derv_w 

def derv_objfunc_b(w_vec,b,x,y):            
    L_derv_b = 0
    for i in range(0, x.shape[0]):
        if y[i]*(np.dot(w_vec,x[i]) + b) < 1:
                 L_derv_b = L_derv_b -y[i]
    return  C* L_derv_b 
    
    
    
            

    
    



if __name__  == '__main__':
    start_time = time.time()
    main()
    end_time = time.time()
    print("Elapsed time = {}".format(end_time - start_time))
