#!/usr/bin/python
#Author Narendra Chennamsetty
#SUNET narench@stanford.edu

import numpy as np
import random as rand
import math
import time

C = 100
def main():
    x = np.loadtxt('HW4-q1/features.txt', delimiter=',')
    y = np.loadtxt('HW4-q1/target.txt')
    indexes = np.arange(0,x.shape[0])
    np.random.shuffle(indexes)
    b = 0
    w_vec = np.zeros(x.shape[1])
    eta = 0.0001
    
    k = 1
    delta_cost = 10.0
    delta_cost_prev =0
    
    i = 1
    while delta_cost > 0.001:
        
        wnew = w_vec - eta*derv_objfunc_w(w_vec,b,x[i],y[i])
        b = b - eta*derv_objfunc_b(w_vec,b,x[i],y[i])
        fk = objfunc(w_vec,b,x,y)
        print(fk)

        if k > 1:
            delta_percent_cost = abs(fk_prev - fk)*100/fk_prev
            delta_cost = 0.5*delta_cost_prev + 0.5*delta_percent_cost
            delta_cost_prev = delta_cost
            
        i = indexes[k] % x.shape[0] 
        #print("index = {}".format(i))
        fk_prev = fk
            
        k+=1
        w_vec = wnew
        
    print("Iterations = {}, value = {}".format(k,fk))    

def objfunc(w_vec,b,x,y):
    total = 0
    for i in range(0, x.shape[0]):
        t = y[i]*(np.dot(w_vec,x[i]) + b)
        if t < 1:
            total += (1 -t)
    return 0.5*sum(w_vec**2) + C*total
    

def derv_objfunc_w(w_vec,b,x_i,y_i):
    
    L_derv_w = np.zeros(w_vec.shape)

    if y_i*(np.dot(w_vec,x_i) + b) < 1:
        L_derv_w = -1*y_i*x_i
    return w_vec +C* L_derv_w 

def derv_objfunc_b(w_vec,b,x_i,y_i):            
    L_derv_b = 0
    if y_i*(np.dot(w_vec,x_i) + b) < 1:
        L_derv_b =  -1*y_i
    return  C* L_derv_b 
    


if __name__  == '__main__':
    start_time = time.time()
    main()
    end_time = time.time()
    print("Elapsed time = {}".format(end_time - start_time))