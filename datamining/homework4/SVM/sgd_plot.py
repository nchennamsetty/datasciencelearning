#!/usr/bin/python
#Author Narendra Chennamsetty
#SUNET narench@stanford.edu

import numpy as np
import random as rand
import math
import matplotlib.pyplot as plt


def main():
    x = np.loadtxt('HW4-q1/features.train.txt', delimiter=',')
    y = np.loadtxt('HW4-q1/target.train.txt')
    
    x_test = np.loadtxt('HW4-q1/features.test.txt', delimiter=',')
    y_test = np.loadtxt('HW4-q1/target.test.txt')
    
    indexes = np.arange(0,x.shape[0])
    np.random.shuffle(indexes)
    
    C_vals = [1,10,50,100,200,300,400,500]
    error_percents = []
    for c in C_vals:
        b = 0
        w_vec = np.zeros(x.shape[1])
        eta = 0.0001
        
        k = 1
        delta_cost = 10.0
        delta_cost_prev =0
        
        i = 1
        while delta_cost > 0.001:
            
            wnew = w_vec - eta*derv_objfunc_w(w_vec,b,x[i],y[i], c)
            b = b - eta*derv_objfunc_b(w_vec,b,x[i],y[i], c)
            fk = objfunc(w_vec,b,x,y, c)
            #print("fk = {}, delta_cost = {}".format(fk, delta_cost))

            if k > 1:
                delta_percent_cost = abs(fk_prev - fk)*100/fk_prev
                delta_cost = 0.5*delta_cost_prev + 0.5*delta_percent_cost
                delta_cost_prev = delta_cost
                
            i = indexes[k % x.shape[0]]  
            #print("index = {}".format(i))
            fk_prev = fk
                
            k+=1
            w_vec = wnew
        
        errors = 0
        for t in range(0, x_test.shape[0]):
            if y_test[t]*(np.dot(w_vec,x_test[t]) + b) < 0:
                errors+=1
        error_percents.append(errors/x.shape[0])
        print("Error Percentage = {}% Iterations = {}, Obj Func Value = {} C = {}".format(errors/x.shape[0], k,fk,c))    
    plt.plot(C_vals, error_percents)
    plt.title("C vs Error Percentages")
    plt.xlabel("C")
    plt.ylabel("Error Percentage")
    plt.show()
    
def objfunc(w_vec,b,x,y,C):
    total = 0
    for i in range(0, x.shape[0]):
        t = y[i]*(np.dot(w_vec,x[i]) + b)
        if t < 1:
            total += (1 -t)
    return 0.5*sum(w_vec**2) + C*total
    

def derv_objfunc_w(w_vec,b,x_i,y_i,C):
    
    L_derv_w = np.zeros(w_vec.shape)

    if y_i*(np.dot(w_vec,x_i) + b) < 1:
        L_derv_w = -1*y_i*x_i
    return w_vec +C* L_derv_w 

def derv_objfunc_b(w_vec,b,x_i,y_i,C):            
    L_derv_b = 0
    if y_i*(np.dot(w_vec,x_i) + b) < 1:
        L_derv_b =  -1*y_i
    return  C* L_derv_b 
    


if __name__  == '__main__':
   main()
