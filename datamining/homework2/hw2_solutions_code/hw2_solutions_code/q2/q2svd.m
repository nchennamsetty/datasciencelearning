A = textread('Matrix.txt');
k = [197; 247; 297; 347; 397; 447; 497];
r = zeros(1,7);
for i = 1:7
    [U S V] = svd(A, 'econ');
    Sk = S(1:k(i),1:k(i));
    Uk = U(:,1:k(i));
    Vk = V(:,1:k(i));    
    Ak = Uk*Sk*Vk';
    
    normA = Ak./(sum(Ak.^2, 2).^0.5 * ones(1, size(Ak,2))); % normlize A
    bball = normA(1:100,:);
    bbCosim = bball * bball';
    num = sum(sum(bbCosim - diag(diag(bbCosim))))/ (2* nchoosek(100,2));
    allCosim = normA * normA';
    deno = sum(sum(allCosim - diag(diag(allCosim))))/ (2* nchoosek(497,2));
    r(i) = num / deno;
end
plot(k, r, '-o')
xlabel('k'), ylabel('r'), title('r vs k plot');
print -deps rdplot