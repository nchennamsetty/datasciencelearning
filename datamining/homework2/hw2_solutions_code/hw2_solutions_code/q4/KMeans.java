package edu.stanford.cs246.hw2;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Algorithm:
 * 
 * Map: 
 * setup(){ 
 * 	Read the centroids 
 * } 
 * map(point) {
 * 
 * 	for each point: 
 * 		Find the nearest centroid 
 * 		emit nearest centroid :
 * 			<k,v> = <centroid id, point> 
 * 		emit cost:
 * 			 <k,v> = <-1, min_distance ^2 >
 * }
 * 
 * Reduce: 
 * setup(){ 
 * 	Read the centroids 
 * } 
 * 
 * reduce(key, points = [list of points]){ 
 * 	centroid_running_average = 0 
 * 	cost = 0 
 * 
 * 		for each point in points:
 *   		if key  == -1 :
 *   			add value to cost
 *   		else :
 * 				add point to centroid_running_average
 *  
 * 	emit cost : 
 * 		<k,v> = <C+centroid id, cost> 
 * 	emit new centroid : 
 * 		<k,v> = <K+centroid id, centroid_running_average/no of points> 
 * }
 * 
 * Main (centroid init file, input path, output dir){
 * 		for i in #jobs: 
 * 			if first iteration: 
 * 				pass centroid file path to mapper 
 * 				Run the job to get output in outputdir/i 
 * 			else: 
 * 				Merge all output in outputdir/(i-1) on HDFS to create a centroid(i) file 
 * 				pass centroid file path to mapper 
 * 				Run the job to get output in outputdir/i
 * }
 * 
 * @author sa
 * 
 */
public class KMeans {

	public static String CFILE = "cFile";
	public static DecimalFormat df = new DecimalFormat("#.#####");

	
	
	public static class CentroidMapper extends
			Mapper<Object, Text, IntWritable, Text> {

		public ArrayList<double[]> INIT = new ArrayList<double[]>();

		@Override
		protected void setup(Context context) throws IOException,
				InterruptedException {

			// Get the centroids and keep them in memory
			FileSystem fs = FileSystem.get(context.getConfiguration());

			Path cFile = new Path(context.getConfiguration().get(CFILE));
			DataInputStream d = new DataInputStream(fs.open(cFile));
			BufferedReader reader = new BufferedReader(new InputStreamReader(d));
			String line;
			while ((line = reader.readLine()) != null) {
				if (!line.startsWith("C")) {

					INIT.add(parsePoint(line));
				}
			}
			reader.close();

		}

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {

			// 1. Get point and parse it
			double[] point = parsePoint(value.toString());

			// 2. Get distance of point from each centroid
			int closestCentroid = 0;
			double distance = Long.MAX_VALUE;

			for (double[] centroid : INIT) {
				double tmp = distance(centroid, point);

				if (tmp < distance) {
					closestCentroid = INIT.indexOf(centroid);
					distance = tmp;
				}
			}

			// 3. Find the closest centroid for the point

			// 4. Emit cluster id and point
			 context.write(new IntWritable(closestCentroid),
					 new Text(longArrayToString(point)));
			context.write(new IntWritable(-1), new Text(df.format(Math.pow(distance, 2))));
		}

	}

	public static class CentroidReducer extends
			Reducer<IntWritable, Text, Text, Text> {

		public String KEY = "k";
		public ArrayList<double[]> INIT = new ArrayList<double[]>();

		@Override
		protected void setup(Context context) throws IOException,
				InterruptedException {

			// Get the centroids and keep them in memory

			FileSystem fs = FileSystem.get(context.getConfiguration());
			Path cFile = new Path(context.getConfiguration().get(CFILE));
			DataInputStream d = new DataInputStream(fs.open(cFile));
			BufferedReader reader = new BufferedReader(new InputStreamReader(d));
			String line;
			while ((line = reader.readLine()) != null) {
				if (!line.startsWith("C")) {
					INIT.add(parsePoint(line));
				}
			}
			reader.close();

		}

		@Override
		protected void reduce(IntWritable key,
				Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			//System.out.println("Key :"+ key.get());
			if (key.get() == -1) {
				double cost = 0;
				int no = 0;

				// Get average for all dimensions and cost too !
				for (Text str : values) {
						no ++;
						cost = cost + Double.parseDouble(str.toString());

				}
				System.out.println("NO of cost data points: "+no);
				System.out.println("Cost : "+ df.format(cost));
				context.write(new Text("C" + "-" + key.toString()), new Text(df.format(cost)));
				
			} else {

				double[] average = new double[INIT.get(0).length];
				int count = 0;
				// double cost = 0;

				// Get average for all dimensions and cost too !
				for (Text str : values) {

					double[] point = parsePoint(str.toString());

					for (int i = 0; i < point.length; i++) {
						// Average for new centroid
						average[i] =  average[i]+point[i];
					}

					count++;
				}

				// New centroid at center of mass for this cluster
				for (int i = 0; i < average.length; i++) {
					average[i] = average[i] / count;
				}

				// Emit new centroid
				String result = longArrayToString(average);
				context.write(new Text(KEY + "-" + key.toString()), new Text(
						result));


			}

		}

	}

	public static void main(String[] args) throws Exception {

		Configuration conf = new Configuration();

		FileSystem fs = FileSystem.get(conf);
		String inputDir = args[0];
		String opDirBase = args[1];
		String initCentroidsPath = args[2];
		int no_of_iters = Integer.parseInt(args[3]);
		String cDir = "";

		for (int i = 0; i <= no_of_iters; i++) {

			System.out.println("Iteration :" + i
					+ "===========================================");
			// Output dir in HDFS for this iteration
			String outputDir = opDirBase + "/" + i;
			// System.out.println("outputDir "+i+" :"+outputDir);
			String inPath = initCentroidsPath;

			// Merge o/p from previous job for jobs after the init run is
			// complete
			if (i > 0) {
				cDir = opDirBase + "/" + (i - 1);
				// System.out.println("cDir "+i+" :"+cDir);
				Path inDir = new Path(cDir);

				inPath = opDirBase + "/c/" + i + "-centroid.txt";
				// Centroid file name for this iteration
				FileUtil.copyMerge(fs, inDir, fs, new Path(inPath), false,
						conf, "");
			}

			// Set centroid path
			conf.set(CFILE, inPath);
			// System.out.println(conf.get(CFILE));

			// Job Params
			Job job = new Job(conf, "KMeans");

			job.setJarByClass(edu.stanford.cs246.hw2.KMeans.class);

			job.setMapperClass(CentroidMapper.class);
			job.setReducerClass(CentroidReducer.class);

			job.setMapOutputKeyClass(IntWritable.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(Text.class);

			FileInputFormat.addInputPath(job, new Path(inputDir));
			FileOutputFormat.setOutputPath(job, new Path(outputDir));

			job.waitForCompletion(true);

		}

	}

	public static double[] parsePoint(String input) {

		if (input.startsWith("k")) {
			String[] tk = input.split("\t");
			input = tk[1];
		}

		String[] tokens = input.split(" ");

		double[] point = new double[tokens.length];

		for (int i = 0; i < point.length; i++) {
			point[i] = Double.parseDouble(tokens[i]);
		}

		return point;
	}

	// Return the Euclidean distance between 2 points in r dimensions
	public static double distance(double[] centroid, double[] point) {
		double result = 0;
		for (int i = 0; i < point.length; i++) {
			result = result + (Math.pow((point[i] - centroid[i]), 2));
			// System.out.println(result);
		}
		return Math.sqrt(result);
	}

	public static String longArrayToString(double[] average) {
		String result = new String();
		for (int i = 0; i < average.length; i++) {
			result = result + df.format(average[i]);
			if (i != average.length) {
				result = result + " ";
			}
		}
		return result;
	}

	public static class DoubleArrayWritable extends ArrayWritable {
		public DoubleArrayWritable() {
			super(DoubleWritable.class);
		}

		public DoubleArrayWritable(DoubleWritable[] values) {
			super(DoubleWritable.class, values);
		}
	}

}
