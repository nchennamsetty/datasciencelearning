import math
import operator
from sys import argv

if len(argv) > 1:
    vocab_file = open(argv[1])
    data = open(argv[2])
    clusters = open(argv[3])
    op = open(argv[4])
else:
    vocab_file = open("vocab.txt")
    data = open("data.txt")
    clusters = open("centroid.txt")
    op = open("top-10-tags.txt",'w')


# L2 distance
def L2(a,b):
    sum = 0
    for i in xrange(len(a)):
        sum = sum + (float(a[i]) - float(b[i]) ) ** 2 
        
    return math.sqrt(sum)

vocab = []
# Read vocab and centroid data
for word in vocab_file:
    vocab.append(word.strip())

centroids = []
for cluster in clusters:
        centroids.append(cluster.split()[1:])

# Assign points to clusters 
cmap = {}
for data_idx, line in enumerate(data):
    cluster = 0
    point = line.strip().split()

    dist = 1000000
    i = 0 
    for c_idx,centroid in enumerate(centroids):
        #print L2(point,centroid)
        if L2(point,centroid) < dist: 
            dist = L2(point,centroid)
            cluster = c_idx
    
    #print "Point: %s Cluster: %s "%(data_idx, cluster)   
    
    if not cmap.has_key(cluster) : cmap[cluster] = []  
    cmap[cluster].append(data_idx)


# Pretty printing
print "\n".join(["Cluster-%s    %s"%(str(k),str(v)) for k,v in cmap.iteritems()])

# Get top 10 tags for each centroid
tags = {}

for idx,c in enumerate(centroids):
    
    # Get tags for clusters from vocab
    ct = {float(c[i]):vocab[i] for i in xrange(len(c))}
    print [ vocab[i].strip() + ":" + c[i] for i in xrange(len(c))]
    # Sort them according to feature weights
    cs = sorted(ct.iteritems(), key = operator.itemgetter(0))
    cs.reverse()
    #print cs
    # Get top 10 tags
    csp = "\n".join([v.strip()+":"+str(k) for k,v in cs[0:10]])
    #print csp
    print "Cluster-%s: %s \n"%(idx, csp)
    
    tags[idx] = csp

# Top tags for document 2
document_id = 2

for k,v in cmap.iteritems():
    if (document_id - 1) in v:
        print "Document %d found in cluster %d." % (document_id, k)
        print "Tags for document %d : \n%s" % (document_id, tags[k])
output = "Tags for document %d :\n%s" % (document_id, tags[k])
op.write(output)
op.close()



