%Question 1
%Name:Tongda Zhang

%input the original data
file1='alex.txt';
 [Alex] = load(file1);
 file2='shows.txt';
 [Item] = importdata(file2);
 file3='user-shows.txt';
[R] = importdata(file3);


R=sparse(R);%change R to sparse matrix to save memory
P=sum(R,2);
P=P.^-0.5;
PR=sparse(diag(P)*R);
clear P;
Cuu=PR*(PR)';
clear PR;
Cuu=Cuu*R;  %calculate out the recommendation matrix 
temp1=full(Cuu(500,1:100));%only save the part that would be used
clear Cuu;

Q=sum(R);
Q=Q.^-0.5;
RQ=sparse(R*diag(Q));
Cii=R*RQ'*RQ;%calculate out the recommendation matrix 
clear R RQ;
temp2=full(Cii(500,1:100));%only save the part that would be used
clear Cii;
tt1=temp1;
tt2=temp2;

%find out the top 5 recommendation 
most5_1=[];
most5_2=[];
for i=1:5
    [t1,t2]=max(temp1);
    most5_1=[most5_1;Item(t2,1)];
    temp1(1,t2)=-1;
end
most5_1
for i=1:5
    [t1,t2]=max(temp2);
    most5_2=[most5_2;Item(t2,1)];
    temp2(1,t2)=-1;
end
most5_2

%find out precision at top k
temp1=tt1;
temp2=tt2;
most19_1=[];
real_1=[];
most19_2=[];
real_2=[];
precision_1=[];
precision_2=[];
for i=1:19
    [t1,t2]=max(temp1);
    most19_1=[most19_1;Item(t2,1)];
    temp1(1,t2)=-1;
    if Alex(1,t2)==1
       real_1=[real_1;1];
    else
       real_1=[real_1;0];
    end
    precision_1=[precision_1;sum(real_1(1:i,1))/i];
end
for i=1:19
    [t1,t2]=max(temp2);
    most19_2=[most19_2;Item(t2,1)];
    temp2(1,t2)=-1;
    if Alex(1,t2)==1
       real_2=[real_2;1];
    else
       real_2=[real_2;0];
    end
    precision_2=[precision_2;sum(real_2(1:i,1))/i];
end
%draw the picture
k=1:19;
k=k';
plot(k,precision_1);
hold on;
plot(k,precision_2);