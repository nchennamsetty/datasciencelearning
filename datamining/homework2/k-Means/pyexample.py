from pylab import plot,show
from numpy import vstack,array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq
from numpy import linalg as lg

# data generation
data = vstack((rand(10,2) + array([.5,.5]),rand(10,2)))

# computing K-Means with K = 2 (2 clusters)
# centroids,_ = kmeans(data,2)
badinit = array([[ 0.2,  0.2],
       [ 1,  1]])
centroids,_ = kmeans(data, badinit)
# assign each sample to a cluster
idx,_ = vq(data,centroids)

# some plotting using numpy's logical indexing
# plot(data[idx==0,0],data[idx==0,1],'ob',
#      data[idx==1,0],data[idx==1,1],'or')
# plot(centroids[:,0],centroids[:,1],'sg',markersize=8)

cost = 0
i=0
for c in centroids:
	cost += lg.norm(c-data[idx[i]])
	i = i+1

print(cost)