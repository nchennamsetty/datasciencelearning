import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.hdfs.*;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.commons.math.linear.*;

public class KmeansHelper {
	
	public static ArrayRealVector getVectorFromline(String line){
		ArrayRealVector vector;
		String[] tokens = line.split("\\s");
		double[] d = new double[tokens.length];
		for (int i=0; i<tokens.length; i++) {
			d[i] = Double.parseDouble(tokens[i]);
		
		}
		vector = new ArrayRealVector(d);
		return vector;
	}
	
	public static String getLineFromVector(ArrayRealVector vect){
		String line;
		StringBuilder sbr  = new StringBuilder();
		for (Double d: vect.toArray() ){
			sbr.append(String.valueOf(d)).append(" ");
		}
		return sbr.toString();
	}
	
	
	

}
