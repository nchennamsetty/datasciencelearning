import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;

/* 
 * MapReduce jobs are typically implemented by using a driver class.
 * The purpose of a driver class is to set up the configuration for the
 * MapReduce job and to run the job.
 * Typical requirements for a driver class include configuring the input
 * and output data formats, configuring the map and reduce classes,
 * and specifying intermediate data formats.
 * 
 * The following is the code for the driver class:
 */
public class KmeansMapRed {

	public static void main(String[] args) throws Exception {

		/*
		 * The expected command-line arguments are the paths containing input
		 * and output data. Terminate the job if the number of command-line
		 * arguments is not exactly 2.
		 */
		if (args.length != 2) {
			System.out.printf("Usage: KmeansMapRed <input dir> <output dir>\n");
			System.exit(-1);
		}

		String centroidsdir = "hdfs:///user/cloudera/KmeansMapReduce";

		for (int i = 1; i <= 20; i++) {

			Job job = new Job();

			job.setJarByClass(KmeansMapRed.class);
			job.setJobName("K-means Mapper");

			FileInputFormat.setInputPaths(job, new Path(args[0]));

			FileOutputFormat.setOutputPath(job,
					new Path(args[1] + String.valueOf(i)));
			if (i == 1) {
				job.getConfiguration().set("centroids.path",
						centroidsdir + "/c2.txt");
			} else {
				job.getConfiguration().set("centroids.path",
						args[1] + String.valueOf(i - 1) + "/part-r-00000");

			}

			job.setMapperClass(ClusterMapper.class);
			job.setReducerClass(ClusterReducer.class);

			job.setOutputKeyClass(IntWritable.class);
			job.setOutputValueClass(Text.class);

			boolean success = job.waitForCompletion(true);
		}

		// System.exit(success ? 0 : 1);
	}
}