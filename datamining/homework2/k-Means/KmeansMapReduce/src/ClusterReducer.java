import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.hdfs.*;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.commons.math.linear.*;

/* 
 * To define a reduce function for your MapReduce job, subclass 
 * the Reducer class and override the reduce method.
 * The class definition requires four parameters: 
 *   The data type of the input key (which is the output key type 
 *   from the mapper)
 *   The data type of the input value (which is the output value 
 *   type from the mapper)
 *   The data type of the output key
 *   The data type of the output value
 */
public class ClusterReducer extends Reducer<IntWritable, Text, Text, Text> {

	/*
	 * The reduce method runs once for each key received from the shuffle and
	 * sort phase of the MapReduce framework. The method receives a key of type
	 * Text, a set of values of type IntWritable, and a Context object.
	 */
	private double cost = 0;
	@Override
	protected void cleanup(Context context) throws IOException,
			InterruptedException {
		
		System.out.println("Cost = " + String.valueOf(cost));
		super.cleanup(context);
		
	}

	@Override
	public void reduce(IntWritable key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {

		/*
		 * For each value in the set of values passed to us by the mapper:
		 */
		if (key.get() != 999) {

			Vector<ArrayRealVector> points = new Vector<ArrayRealVector>();
			for (Text value : values) {

				ArrayRealVector point = KmeansHelper.getVectorFromline(value
						.toString());
				points.add(point);
				//System.out.println(value.toString());
			}
			ArrayRealVector centroid = points.firstElement();
			for (int i = 1; i < points.size(); i++) {
				centroid = centroid.add(points.get(i));

			}

			centroid.mapDivideToSelf(points.size());
			String line = KmeansHelper.getLineFromVector(centroid);

			/*
			 * Call the write method on the Context object to emit a key and a
			 * value from the reduce method.
			 */
			context.write(new Text(line), new Text(""));
		}
		
		else{
			
		for(Text value: values){
			cost += Double.parseDouble(value.toString());
		}
		
		
		}
	}
	
	

}