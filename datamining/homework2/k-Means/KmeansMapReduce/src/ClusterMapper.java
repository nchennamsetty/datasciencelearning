import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.hdfs.*;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.commons.math.linear.*;

/* 
 * To define a map function for your MapReduce job, subclass 
 * the Mapper class and override the map method.
 * The class definition requires four parameters: 
 *   The data type of the input key
 *   The data type of the input value
 *   The data type of the output key (which is the input key type 
 *   for the reducer)
 *   The data type of the output value (which is the input value 
 *   type for the reducer)
 */

public class ClusterMapper extends
		Mapper<LongWritable, Text, IntWritable, Text> {

	/*
	 * The map method runs once for each line of text in the input file. The
	 * method receives a key of type LongWritable, a value of type Text, and a
	 * Context object.
	 */

	private ArrayList<ArrayRealVector> _centroids;

	private ArrayList<ArrayRealVector> getCentroids(Context context)
			throws IOException {
		if (_centroids == null) {

			String line;
			_centroids = new ArrayList<ArrayRealVector>();
			ArrayRealVector centroid;

			Path path_centrd = new Path(context.getConfiguration().get(
					"centroids.path"));
			//System.out.println(context.getConfiguration().get("centroids.path"));
			FileSystem fs = FileSystem.get(context.getConfiguration());
			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(path_centrd)));
			try {

				// Construct centroid vectors
				while ((line = br.readLine()) != null) {
					centroid = KmeansHelper.getVectorFromline(line);
					_centroids.add(centroid);
				}
			} catch (Exception ex) {
				System.out.print(ex.getCause());

			} finally {

				br.close();
			}

		}
		return _centroids;

	}

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {

		// Load the centroid file
		ArrayList<ArrayRealVector> centroids = getCentroids(context);
		ArrayRealVector point = KmeansHelper
				.getVectorFromline(value.toString());
		Vector<Double> distances = new Vector<Double>();
		int nearestCentroid_idx;

		for (ArrayRealVector c : centroids) {

			distances.add(c.getDistance(point));

		}

		nearestCentroid_idx = getMinIndex(distances);
		context.write(new IntWritable(nearestCentroid_idx + 1), value);

		// Calculating cost
		ArrayRealVector nearestCentroid = centroids.get(nearestCentroid_idx);
		
		context.write(new IntWritable(999),
				new Text(String.valueOf(nearestCentroid.getDistance(point))));

	}

	private int getMinIndex(Vector<Double> d) {
		double currValue = d.firstElement();
		int minindex = 0;

		for (int i = 1; i < d.size(); i++) {

			if (d.get(i) < currValue) {
				currValue = d.get(i);
				minindex = i;

			}
		}
		return minindex;
	}
}
