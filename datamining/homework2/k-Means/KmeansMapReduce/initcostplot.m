% Author Narendra Chennamsetty
% SUNET ID narench@stanford.edu
clear all;
close all;

iters = [ 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20]';
c1costs = load('c1costs.txt');
c2costs = load('c2costs.txt');
plot(iters, c1costs,'r',iters, c2costs, 'b');
xlabel('Iterations');ylabel('Cost'); legend('c1', 'c2');
title("K-means: Cost over Iteration");
