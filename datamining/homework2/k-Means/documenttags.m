% Author Narendra Chennamsetty
% SUNET ID narench@stanford.edu

centroids = load('c1-part-r-00000.txt');
secdoc = [0.21 0.28 0.5 0 0.14 0.28 0.21 0.07 0 0.94 0.21 0.79 0.65 0.21 0.14 0.14 0.07 0.28 3.47 0 1.59 0 0.43 0.43 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0 0 0 0 0 
0 0 0 0 0 0 0 0.132 0 0.372 0.18 0.048 5.114 101 1028 1]


d = []
for i= 1:size(centroids,1)
	d(i) = norm(secdoc - centroids(i,:));
end

i, v = min(d)
c = centroids(10,:)

[sortedvals, idx] = sort(c, 'descend');
idx

[fid, msg] = fopen('vocab.txt');
V = textscan(fid, '%s');
fclose(fid)

for i = 1:10
V(1){1}(idx(i))
end


ans = 
{
  [1,1] = instillation
}
ans = 
{
  [1,1] = methoxyfenozide
}
ans = 
{
  [1,1] = sodium-sensitive
}
ans = 
{
  [1,1] = post-infectious
}
ans = 
{
  [1,1] = teleological
}
ans = 
{
  [1,1] = neodymium
}
ans = 
{
  [1,1] = anserine
}
ans = 
{
  [1,1] = embl3
}
ans = 
{
  [1,1] = spc
}
ans = 
{
  [1,1] = alkalemia
}
