#!/usr/bin/python
# Author Narendra Chennamsetty
# SUNET Id: narench@stanford.edu
# k-Means on mapreduce

# Overall strategy
# 
# K-means
# init centroid

# mapper:
# for each point p
    # find centroid Cp closest to it
	# assign point to Cp
    # emit (Cp, point)
# reducer: (Cp, points)
  # Cpnew = avg of points
  # emit (Cpnew, points) -- [you can emit (Cpnew) to centroid dir]
  
# firstjob 
 # in = C1 , data.txt
 # out = Cpnew, points
 
 # second job
 
 # in = Cpnew , points
 # out = Cpnew1, points
 
 # third job
 #some useful commands
 #np.linalg.norm(a-a**3)
 #np.argmin(vec)


import mrjob
