% Octave script for Recommendation system%
% Narendra Chennamsetty
% narench@stanford.edu
%
%clear all;
%close all;

R = load('user-shows.txt');
size(R)
% Computing P,Q
Q = zeros(size(R,2));


for i = 1:size(R, 2)
	Q(i,i) = sum(R(:,i));
end


% Calculating S_i = item similarity matrix 
% and S_u = user similarity matrix

X = R*Q^-(1/2);
S_i = X'*X;





% Recommendation matrix Item-Item
Re_ii = R*S_i;
S = Re_ii(500,1:100);
[val, idx] = sort(S, 'descend');

alx = load('alex.txt');


precision = zeros(1,19);
for k = 1:19
	precision(k) = mean(alx(idx(1:k)));
end

% User - User
P = zeros(size(R,1));

for i = 1:size(R, 1)
	P(i,i) = sum(R(i,:));
end
Y = R'*P^(-1/2);
S_u = Y'*Y;

Re_uu = S_u*R;
Snew = Re_uu(500, 1:100);
[val_uu, idx_uu] = sort(Snew, 'descend');
val_uu(1:5)
idx_uu(1:5)
for k = 1:19
precision_uu(k) = mean(alx(idx_uu(1:k)));
end


close all;
plot(1:19, precision, 'r-', 1:19, precision_uu, 'b-');
title('Precision at top-k");
title('Precision at top-k');
xlabel('k');ylabel('Precision');
legend('Item-Item', 'User-User');





