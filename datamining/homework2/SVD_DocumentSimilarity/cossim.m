function retval = cossim(A,B)
	retval = sum(A.*B)/(sqrt(sum(A.^2))*sqrt(sum(B.^2)));
endfunction