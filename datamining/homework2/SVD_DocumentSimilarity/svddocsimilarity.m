% SVD decomposition applied to document similarity %
% Author: Narendra Chennamsetty
% SUNET ID = narench@stanford.edu
% CS246 Homework 2 - Q2b

%clear all;
I = load('Matrix.txt');

% Calculating r
%r = simratio(I);
%fprintf('r = %f\n', r);

k_val = [197 247 297 347 397 447 497];
r_val = zeros(1,7);

[U, S, V] = svd(I, 'econ');


for n = 1:7
	k = k_val(n);
	S_k = S;
	for i = k+1:size(S,2)
		S_k(i,i) = 0;
	end
	I_res = U*S_k*V';
	r_val(n) = simratio(I_res);

end

plot(k_val, r_val);




