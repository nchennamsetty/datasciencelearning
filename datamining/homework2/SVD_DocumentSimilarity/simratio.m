function r = simratio(I)
	bb_sim = 0;
	doc_sim = 0;
	for i = 1:99
		for j = i+1:100
			d_i = I(i,:);
			d_j = I(j,:);
			bb_sim = bb_sim + cossim(d_i, d_j);
		end
	end
	bb_sim = bb_sim/nchoosek(100,2);
	
	for i = 1:496
		for j = i+1:497
			d_i = I(i,:);
			d_j = I(j,:);
			doc_sim = doc_sim + cossim(d_i, d_j);
		end
	end
	
	doc_sim = doc_sim/nchoosek(497,2);
	r = bb_sim/doc_sim;

endfunction