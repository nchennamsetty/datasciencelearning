
from networkx.algorithms import bipartite
import networkx as nx
import itertools as it
import copy

BG = nx.DiGraph()
BG.add_nodes_from(['cameras','phones', 'printers'], bipartite=0)
BG.add_nodes_from(['hp', 'nokia', 'kodak', 'apple', 'canon'], bipartite=1)
BG.add_edge('cameras', 'nokia')
BG.add_edge('cameras', 'kodak')
BG.add_edge('cameras', 'canon')
BG.add_edge('phones', 'nokia')
BG.add_edge('phones', 'apple')
BG.add_edge('printers', 'hp')

A = ['cameras', 'phones', 'printers']
La = list(it.combinations(A,2))

B = ['hp', 'nokia', 'kodak', 'apple', 'canon']
Lb = list(it.combinations(B,2))

Sa = {}
for i in La:
    Sa[i] = 0
Sa[('cameras', 'cameras')] = 1
Sa[('phones', 'phones')] = 1
Sa[('printers', 'printers')] = 1


Sb = {}
for i in Lb:
    Sb[i] = 0

Sb[('apple', 'apple')] = 1
Sb[('nokia', 'nokia')] = 1
Sb[('hp', 'hp')] = 1
Sb[('kodak', 'kodak')] = 1
Sb[('canon', 'canon')] = 1

coeff = 0
for i in range(0,3):
    Sa_prev = copy.deepcopy(Sa)
    for k in Sa.keys():
        (x, y) = k
        if x!=y:
            coeff = (0.8/(len(BG.edges(x)*len(BG.edges(y)))))
            sigma = 0
            for n in BG.neighbors(x):
                for n2 in BG.neighbors(y):
                    key = (n,n2)
                    if key not in Sb.keys():
                        key = key[::-1]
                    sigma +=Sb[key]
            Sa[k] = coeff*sigma
    print("----------------Sa Iteration {}-----".format(i))
    for it in A:
        for sakey in Sa.keys():
            if it in sakey:
                print(sakey, Sa[sakey])    
    #print("SA = {} ".format(sorted(Sa.items(), key=lambda x:x[0])))
        #print("SA Previous - {} , {} = {} ".format(x,y,Sa_prev[k]))

    coeff = 0
    for k in Sb.keys():
        (x, y) = k
        if x!=y:
            coeff = (0.8/(len(BG.in_edges(x)*len(BG.in_edges(y)))))
            sigma = 0
            for n in BG.predecessors(x):
                for n2 in BG.predecessors(y):
                    key = (n,n2)
                    if key not in Sa_prev.keys():
                         key = key[::-1]
                    sigma +=Sa_prev[key]
            Sb[k] = coeff*sigma
    print("----------------Sb Iteration {}-----".format(i))
    for it in B:
        for sbkey in Sb.keys():
            if it in sbkey:
                print(sbkey, Sb[sbkey])                        
    #print("SB = {}".format(sorted(Sb.items(), key=lambda x:x[0])))
