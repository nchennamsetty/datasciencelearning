#!/usr/bin/python
# Author Narendra Chennamsetty
# SUNET narench@stanford.edu

import numpy as np
import numpy.random
import math


def main() :
    # Getting m,n
    m = 0
    n = 0
    with open('ratings.train.txt') as ratings:
         for line in ratings:
             fields = line.split()
             if int(fields[0]) > n:
                n = int(fields[0])
             if int(fields[1]) > m:
                m = int(fields[1])
    k = 20
                    
    #Init P & Q
    P = np.random.rand(n,k)*math.sqrt(5.0/k)
    Q = np.random.rand(m,k)*math.sqrt(5.0/k)
    mu = 0.01
    L = 0.2
    epsilons = []
    for it in range(1,40):
        with open('ratings.train.txt') as ratings_file:
            for line in ratings_file:
                fields = line.split()
                if len(fields) > 2:
                    userid = int(fields[0])
                    movieid = int(fields[1])
                    rating = float(fields[2])
                    #print(userid,movieid,rating)
                    Qi = Q[movieid-1]
                    Pu = P[userid -1]
                    #print(Qi)
                    epsilon = rating - np.dot(Qi,Pu.transpose())
                    Q[movieid-1] = Qi+mu*(epsilon*Pu - L*Qi)
                    P[userid-1] = Pu+mu*(epsilon*Qi - L*Pu)
                    #print "epsilon= {0}".format(epsilon)

        # Error calculation
        Error = 0
        reg_term = L*(sum(sum(P**2)) + sum(sum(Q**2)))
        with open('ratings.train.txt') as ratings_file:
            for line in ratings_file:
                fields = line.split()
                if len(fields) > 2:
                    userid = int(fields[0])
                    movieid = int(fields[1])
                    rating = int(fields[2])
                    
                   
                    Error = Error + (rating - np.dot(Q[movieid-1],P[userid-1].transpose()))**2

        Error = Error + reg_term
        print("Error= {} , mu = {}".format(Error, mu))
	
if __name__  == '__main__':
   main()
