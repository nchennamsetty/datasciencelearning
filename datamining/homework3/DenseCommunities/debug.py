#!/usr/bin/python
import pickle

S = pickle.load(open('Sdump.txt', 'rb'))

induced_edge_count = 0
deg = {}
with open('livejournal-undirected.txt') as g:
	for line in g:
		fields = line.split('\t')
		v1 = int(fields[0])
		v2 = int(fields[1])

		if v1 in S and v2 in S:
			induced_edge_count +=1

			if v1 in deg.keys():
				deg[v1] += 1
			else:
				deg[v1] = 1
				
			if v2 in deg.keys():
				deg[v2] += 1
			else:
				deg[v2] = 1

print(len(deg))
print(len(S))