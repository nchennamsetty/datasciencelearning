# Author Narendra Chennamsetty
# SUNET Id - narench@stanford.edu
import copy
import matplotlib.pyplot as plt
import math

def main():
    V = set()
    epsilon = [0.1, 0.5, 1, 2]
    deg = {}
    E_S = 0
    density_S = 0
    with open('livejournal-undirected.txt') as g:
        for line in g:
            fields = line.split()
            v1 = int(fields[0])
            v2 = int(fields[1])
            E_S+=1
            V.add(v1)
            V.add(v2)
            if v1 in deg.keys():
                deg[v1] += 1
            else:
                deg[v1] = 1
            if v2 in deg.keys():
                deg[v2] += 1
            else:
                deg[v2] = 1

   
    dst = []
    ds = []
    iters = [0,0,0,0]
    for t in range(0,4):
        S = copy.deepcopy(V)
        St = copy.deepcopy(V)
    
        (density_S, deg)  = get_density(S)
        density_St = 0
        while len(S) > 1:
            A_S = set()
            iters[t] +=1
            for i in S:
                if deg[i] <= 2*(1+epsilon[t])*density_S:
                    A_S.add(i)
            S = S - A_S
                    
            if S:
                (density_S, deg) = get_density(S)
                        
                if density_S > density_St:
                    St = copy.deepcopy(S)
                    density_St = density_S
            
        print("Dense Subgraph size = {} for e = {} Actual Iterations = {} Theoretical iterations = {}".format(len(St),epsilon[t], iters[t], math.log(len(V), 1 +epsilon[t])))
    print(iters)
            
def get_density(S):
    """Gets density of subset S nodes"""
    induced_edge_count = 0
    deg = {}
    for v in S:
        deg[v] = 0
    with open('livejournal-undirected.txt') as g:
        for line in g:
            fields = line.split()
            v1 = int(fields[0])
            v2 = int(fields[1])

            if v1 in S and v2 in S:
                induced_edge_count +=1
                deg[v1] += 1
                deg[v2] += 1
                
    return (1.0*induced_edge_count/len(S) , deg)
                
                
        
        
    

if __name__  == '__main__':
    main()
