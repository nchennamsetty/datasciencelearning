# Author Narendra Chennamsetty
# SUNET Id - narench@stanford.edu
import copy
import matplotlib.pyplot as plt

def main():
    V = set()
    epsilon = 0.05
    deg = {}
    E_S = 0
    density_S = 0
    with open('livejournal-undirected.txt') as g:
        for line in g:
            fields = line.split()
            v1 = int(fields[0])
            v2 = int(fields[1])
            E_S+=1
            V.add(v1)
            V.add(v2)
            if v1 in deg.keys():
                deg[v1] += 1
            else:
                deg[v1] = 1
            if v2 in deg.keys():
                deg[v2] += 1
            else:
                deg[v2] = 1

   
    
    density_St = [0]*20
    St = set()
    rho = edges = size = iters =[]
    for c in range(0,1):
        V = V-St
        S = copy.deepcopy(V)
        St = set()
    
        (density_S, deg)  = get_density(S)
        i = 0
        while len(S) > 1:
            A_S = set()
            for i in S:
                if deg[i] <= 2*(1+epsilon)*density_S:
                    A_S.add(i)
            S = S - A_S
                    
            if S:
                (density_S, deg) = get_density(S)
                        
                if density_S > density_St[c]:
                    St = copy.deepcopy(S)
                    density_St[c] = density_S
            i+=1
            iters.append(i)
            rho.append(density_S)
            edges.append(len(S)*density_S)
            print ("density = {} |S| = {} Edges = {}".format(density_S, len(S), density_S*len(S)))
            
        #print("Dense Subgraph = {} for e={} in {} iterations".format(St,epsilon[t], iters[t]))
        # print ("Density of St = {} , Length St = {}, Edges = {}".format(density_St[c], len(St), density_St[c]*len(St)))
        # plt.figure(1)
        # plt.plot(iters, rho)
        # plt.xlabel('i')
        # plt.ylabel('$\rho$')
        # plt.figure(2)
        # plt.plot(iters, size)
        # plt.xlabel('i')
        # plt.ylabel('|S|')
        # plt.figure(3)
        # plt.plot(iters, edges)
        # plt.xlabel('i')
        # plt.ylabel('|E(S)|')
        # plt.show()
def get_density(S):
    """Gets density of subset S nodes"""
    induced_edge_count = 0
    deg = {}
    for v in S:
        deg[v] = 0
    with open('livejournal-undirected.txt') as g:
        for line in g:
            fields = line.split()
            v1 = int(fields[0])
            v2 = int(fields[1])

            if v1 in S and v2 in S:
                induced_edge_count +=1
                deg[v1] += 1
                deg[v2] += 1
                
    return (1.0*induced_edge_count/len(S) , deg)
                
                
        
        
    

if __name__  == '__main__':
    main()
