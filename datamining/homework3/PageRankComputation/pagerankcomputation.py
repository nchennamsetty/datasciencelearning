#!/usr/bin/python
# Author - Narendra Chennamsetty
#SUNET narench@stanford.edu

import numpy as np
import time
import networkx as nx
import random as rand 


def pagerank_poweriteration():
    
    n = 100
    N = np.zeros([n,n])
    M = np.zeros([n,n]) #Transition matrix
    beta = 0.8
    g = open('graph.txt')
    for line in g:
        fields = line.split()
        source = int(fields[0])
        dest = int(fields[1])
        N[dest-1][source-1] = N[dest-1][source-1] + 1
        
    M = N/sum(N)
    r = np.ones(n)*(1.0/n)
    
        
    start_time = time.time()

    for k in range(1,40):
        r = beta*np.dot(M,r) + ((1-beta)/n)*np.ones(n)
            
    end_time = time.time()
    #print(r)        
    print("Power iteration elapsed time = %g seconds" % (end_time - start_time))
    return r

def pagerank_montecarlo():

    # Monte Carlo algorithm
    n = 100
    beta = 0.8
    
    r = np.zeros(n)
    R = 5
    
    G = nx.MultiDiGraph()
    G.add_nodes_from(range(1,n+1))
    
    g = open('graph.txt')
    for line in g:
        fields = line.split()
        source = int(fields[0])
        dest = int(fields[1])
        
        G.add_edge(source, dest)
        
    start_time = time.time()
    for node in G.nodes():
        #print("node ={}".format(node))
        for run in range(1,R+1):
            r[node-1] += 1
            nextnode = node
            while nextnode != -1:
                if rand.random() < beta:
                    nextnode =  rand.choice(G.edges(nextnode))[1]
                    #print("nextnode ={}".format(nextnode))
                    r[nextnode-1] += 1
                else:
                    r[node-1] += 1 #terminate
                    nextnode = -1
    r = r*(1-beta)/(n*R)
    #print(r)
    end_time = time.time()
    print("Monte Carlo elapsed time = {} seconds for R = {}".format((end_time - start_time),R))
    return r
        
    

    

if __name__  == '__main__':
    r_mc = pagerank_montecarlo()
    #print(r_mc)
    #r_pi = pagerank_poweriteration()
    for r in r_mc:
        print(r)
    #error =sum(abs(r_mc - r_pi))/100
    #print("error = {}".format(error))
    

    
    
    
